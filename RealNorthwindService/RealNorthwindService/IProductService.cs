﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Collections;

namespace MyWCFServices.RealNorthwindService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IProductService
    {
        [OperationContract]
        [FaultContract(typeof(ProductFault))]
        Product GetProduct(int id);

        [OperationContract]
        [FaultContract(typeof(ProductFault))]
        ArrayList GetAllProducts();

        [OperationContract]
        [FaultContract(typeof(ProductFault))]
        bool UpdateProduct(Product product, ref string message);




        //Just Created
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        Socio getSocio(int id);
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        SociosArray getAllSocios();
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        SociosArray getSociosWithQuery(string query, string[] parameters, object[] values);

        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        int getNextSocioID();
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        bool updateSocio(Socio socio);
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        bool insertSocio(Socio socio);

        //PARA COPIAS
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        int getNumeroDeCopiasDisponiblesParaPelicula(int id);
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        ArrayCopias getCopiasWithQuery(string query, string[] paramters, string[] values);

        [OperationContract]
        [FaultContract(typeof(UsuarioFault))]
        Usuario getUsuario(string id);
        [OperationContract]
        [FaultContract(typeof(UsuarioFault))]
        ArrayUsuarios getUsuarioWithQuery(string query, string[] parameters, object[] values);

        //PARA PELICULAS
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        ArrayPeliculas getPeliculasWithQuery(string query, string[] parameters, object[] values);

        //PARA TIPOS
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        ArrayCostos getCostosWithQuery(string query, string[] parameters, string[] values);

        //PARA BAJAS
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        ArrayBajas getBajasWithQuery(string query, string[] parameters, string[] values);

        //PARA RESERVAS
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        ArrayReservas getReservasWithQuery(string query, string[] parameters, object[] values);

        //PARA RENTAS
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        ArrayRentas getRentasWithQuery(string query, string[] parameters, object[] values);

        //PARA RENTAS
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        ArrayMultas getMultasWithQuery(string query, string[] parameters, object[] values);

        //PARA EJECUCIÓN
        [OperationContract]
        [FaultContract(typeof(SocioFault))]
        bool executeNonQuery(string query, string[] parameters, object[] values);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "RealNorthwindService.ContractType".
    [DataContract]
    public class Product
    {
        [DataMember]
        public int ProductID { get; set; }
        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string QuantityPerUnit { get; set; }
        [DataMember]
        public decimal UnitPrice { get; set; }
        [DataMember]
        public bool Discontinued { get; set; }
    }
    [DataContract]
    public class ProductFault
    {
        public ProductFault(string msg)
        {
            FaultMessage = msg;
        }


        [DataMember]
        public string FaultMessage;
    }





    //Creado para el proyecto final
    [DataContract]
    public class Socio
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public string numero { get; set; }

        [DataMember]
        public string identificacion { get; set; }

        [DataMember]
        public DateTime nacimiento { get; set; }

        [DataMember]
        public string domicilio { get; set; }

        [DataMember]
        public DateTime registro { get; set; }

        [DataMember]
        public string password { get; set; }
    }
    [DataContract]
    public class SocioFault
    {
        public SocioFault(string msg)
        {
            FaultMessage = msg;
        }

        [DataMember]
        public string FaultMessage;
    }
    [DataContract]
    public class SociosArray
    {
        [DataMember]
        public Socio[] socios { get; set; }
    }


    [DataContract]
    public class Usuario
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string tipo { get; set; }

        [DataMember]
        public string password { get; set; }
    }
    [DataContract]
    public class UsuarioFault
    {
        public UsuarioFault(string msg)
        {
            FaultMessage = msg;
        }

        [DataMember]
        public string FaultMessage;
    }

    [DataContract]
    public class Copia
    {
        [DataMember]
        public int idcopia { get; set; }

        [DataMember]
        public int noserie { get; set; }
    }
    [DataContract]
    public class ArrayCopias
    {
        [DataMember]
        public Copia[] copias { get; set; }
    }


    [DataContract]
    public class Pelicula
    {
        [DataMember]
        public int noserie { get; set; }
        [DataMember]
        public string titutlo { get; set; }
        [DataMember]
        public int anodefilmacion { get; set; }
        [DataMember]
        public string directores { get; set; }
        [DataMember]
        public string actoresprincipales { get; set; }
        [DataMember]
        public string idiomaoriginal { get; set; }
        [DataMember]
        public string idiomasdisponibles { get; set; }
        [DataMember]
        public string genero { get; set; }
        [DataMember]
        public float duracion { get; set; }
        [DataMember]
        public string tipo { get; set; }
    }
    [DataContract]
    public class ArrayPeliculas
    {
        [DataMember]
        public Pelicula[] peliculas { get; set; }
    }

    //COSTOS
    [DataContract]
    public class Costo
    {
        [DataMember]
        public string tipo { get; set; }
        [DataMember]
        public float precio { get; set; }
        [DataMember]
        public int tiemporenta { get; set; }
    }
    [DataContract]
    public class ArrayCostos
    {
        [DataMember]
        public Costo[] costos { get; set; }
    }


    //BAJAS
    [DataContract]
    public class Baja
    {
        [DataMember]
        public int idbaja { get; set; }
        [DataMember]
        public int idcopia { get; set; }
        [DataMember]
        public string razon { get; set; }
    }
    [DataContract]
    public class ArrayBajas
    {
        [DataMember]
        public Baja[] bajas { get; set; }
    }

    //RESERVAS
    [DataContract]
    public class Reserva
    {
        [DataMember]
        public int noserie { get; set; }
        [DataMember]
        public int socio { get; set; }
        [DataMember]
        public DateTime fechareserva { get; set; }
        [DataMember]
        public int idreserva { get; set; }
    }
    [DataContract]
    public class ArrayReservas
    {
        [DataMember]
        public Reserva[] reservas { get; set; }
    }

    //RENTAS
    [DataContract]
    public class Renta
    {
        [DataMember]
        public int idcopia { get; set; }
        [DataMember]
        public int idrenta { get; set; }
        [DataMember]
        public int idsocio { get; set; }
        [DataMember]
        public int noserie { get; set; }
        [DataMember]
        public float costo { get; set; }
        [DataMember]
        public string estado { get; set; }
        [DataMember]
        public DateTime fecha { get; set; }
        [DataMember]
        public DateTime fechaentrega { get; set; }
    }

    [DataContract]
    public class ArrayRentas
    {
        [DataMember]
        public Renta[] rentas { get; set; }
    }

    [DataContract]
    public class Multa
    {
        [DataMember]
        public int idmulta { get; set; }
        [DataMember]
        public int idrenta { get; set; }
        [DataMember]
        public float multatotal { get; set; }
    }

    [DataContract]
    public class ArrayMultas
    {
        [DataMember]
        public Multa[] multas { get; set; }
    }

    [DataContract]
    public class ArrayUsuarios
    {
        [DataMember]
        public Usuario[] usuarios { get; set; }
    }
}
