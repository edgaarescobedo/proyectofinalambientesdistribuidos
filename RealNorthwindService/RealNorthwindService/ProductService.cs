﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyWCFServices.RealNorthwindBDO;
using MyWCFServices.RealNorthwindLogic;
using System.Collections;

namespace MyWCFServices.RealNorthwindService
{
 public class ProductService : IProductService
 {
    ProductLogic productLogic = new ProductLogic();
    
    public Product GetProduct(int id)
    {
        ProductBDO productBDO = null;
        try
        {
            productBDO = productLogic.GetProduct(id);
        }
        catch (Exception e)
        {
            string msg = e.Message;
            string reason = "GetProduct Exception";
            throw new FaultException<ProductFault> (new ProductFault(msg), reason);
        }
        if (productBDO == null)
        {
            string msg = string.Format("No product found for id {0}", id);
            string reason = "GetProduct Empty Product";
            if (id == 999)
            {
                throw new Exception(msg);
            }
            else
            {
                throw new FaultException<ProductFault> (new ProductFault(msg), reason);            }
        }
        Product product = new Product();
        TranslateProductBDOToProductDTO(productBDO, product);
        return product;
    }
    public ArrayList GetAllProducts()
    {
        ArrayList arrayRecibido = productLogic.GetAllProducts();
        ArrayList nuevoArrayList = new ArrayList();


        //Transformación de productos
        foreach (ProductBDO p in arrayRecibido) {
            Product product = new Product();
            TranslateProductBDOToProductDTO(p, product);
            nuevoArrayList.Add(product);
        }



        return nuevoArrayList;
    }
    public bool UpdateProduct(Product product, ref string message)
    {
        bool result = true;
        // first check to see if it is a valid price
        if (product.UnitPrice <= 0)
        {
            message = "Price cannot be <= 0";
            result = false;
        }
        // ProductName can't be empty
        else if (string.IsNullOrEmpty(product.ProductName))
        {
            message = "Product name cannot be empty";
            result = false;
        }
        // QuantityPerUnit can't be empty
        else if(string.IsNullOrEmpty(product.QuantityPerUnit))
        {
            message = "Quantity cannot be empty";
            result = false;
        }
        else
        {
            ProductBDO productBDO = new ProductBDO();
            TranslateProductDTOToProductBDO(product,productBDO);
            try
            {
                result = productLogic.UpdateProduct(productBDO, ref message);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                string reason = "UpdateProduct Exception";
                throw new FaultException<ProductFault>(new ProductFault(msg), reason);
            }
        }
        return result;
     }
    private void TranslateProductBDOToProductDTO(ProductBDO productBDO,Product product)
     {
         product.ProductID = productBDO.ProductID;
         product.ProductName = productBDO.ProductName;
         product.QuantityPerUnit = productBDO.QuantityPerUnit;
         product.UnitPrice = productBDO.UnitPrice;
         product.Discontinued = productBDO.Discontinued;
     }
    private void TranslateProductDTOToProductBDO(Product product,ProductBDO productBDO)
     {
         productBDO.ProductID = product.ProductID;
         productBDO.ProductName = product.ProductName;
         productBDO.QuantityPerUnit = product.QuantityPerUnit;
         productBDO.UnitPrice = product.UnitPrice;
         productBDO.Discontinued = product.Discontinued;
     }


     //Creado para el proyecto final
     //SOCIO
     private void transalteSocioBDOtoSocioDTO(SocioBDO socioBDO, Socio socio)
     {
         socio.id = socioBDO.id;
         socio.nombre = socioBDO.nombre;
         socio.numero = socioBDO.numero;
         socio.identificacion = socioBDO.identificacion;
         socio.nacimiento = socioBDO.nacimiento;
         socio.domicilio = socioBDO.docimicilio;
         socio.registro = socioBDO.registro;
         socio.password = socioBDO.password;
     }
     private void translateSocioDTOtoSocioBDO(Socio socio, SocioBDO socioBDO)
     {
         socioBDO.id = socio.id;
         socioBDO.nombre = socio.nombre;
         socioBDO.numero = socio.numero;
         socioBDO.identificacion = socio.identificacion;
         socioBDO.nacimiento = socio.nacimiento;
         socioBDO.docimicilio = socio.domicilio;
         socioBDO.registro = socio.registro;
         socioBDO.password = socio.password;
     }
     public Socio getSocio(int id)
     {
         SocioBDO socioBDO = null;
         try
         {
             socioBDO = productLogic.getSocio(id);
         }
         catch (Exception e)
         {
             string msg = e.Message;
             string reason = "getSocio Exception";
             throw new FaultException<SocioFault>(new SocioFault(msg), reason);
         }
         if (socioBDO == null)
         {
             string msg = string.Format("No socio found for id {0}", id);
             string reason = "getSocio Empty Socio";
         }
         Socio socio = new Socio();
         transalteSocioBDOtoSocioDTO(socioBDO, socio);
         return socio;

     }
     public SociosArray getAllSocios()
     {
         Socio[] socios;
         try
         {
             ArrayList entrante = productLogic.getAllSocios();
             socios = new Socio[entrante.Count];

             for (int i = 0; i < socios.Length; ++i)
             {
                 Socio s = new Socio();
                 transalteSocioBDOtoSocioDTO((SocioBDO)entrante[i],s);
                 socios[i] = s;
             }
         }
         catch (Exception e)
         {
             string msg = e.Message;
             string reason = "getAllSocios Exception";
             throw new FaultException<SocioFault>(new SocioFault(msg), reason);
         }
         if (socios.Length == 0)
         {
             throw new FaultException<SocioFault>(new SocioFault("No se encontraron socios"), "No se encontraron socios en la base de datos");
         }

         SociosArray sa = new SociosArray();
         sa.socios = socios;
         return sa;

     }
     public int getNextSocioID()
     {
         return productLogic.getNextSocioID();
     }
     public bool updateSocio(Socio socio)
     {
         SocioBDO s = new SocioBDO();
         translateSocioDTOtoSocioBDO(socio, s);
         return productLogic.updateSocio(s);
     }
     public bool insertSocio(Socio socio)
     {
         SocioBDO s = new SocioBDO();
         translateSocioDTOtoSocioBDO(socio, s);
         return productLogic.insertSocio(s);
     }

     public SociosArray getSociosWithQuery(string query, string[] parameters, object[] values)
     {
         //Se hace la conversión
         ArrayList resultado = productLogic.getSociosWithQuery(query, parameters, values);
         Socio[] socios = new Socio[resultado.Count];

         for (int i = 0; i < socios.Length; ++i)
         {
             Socio s = new Socio();
             transalteSocioBDOtoSocioDTO((SocioBDO)resultado[i], s);
             socios[i] = s;
         }

         SociosArray sa = new SociosArray();
         sa.socios = socios;
         return sa;
     }

     private void translateUsuarioBDOtoUsuarioDTO(UsuarioBDO usuarioBDO, Usuario usuario)
     {
         usuario.id = usuarioBDO.id;
         usuario.password = usuarioBDO.password;
         usuario.tipo = usuarioBDO.tipo;
     }
     private void translateUsuarioDTOtoUsuarioBDO(Usuario usuario, UsuarioBDO usuarioBDO)
     {
         usuarioBDO.id = usuario.id;
         usuarioBDO.password = usuario.password;
         usuarioBDO.tipo = usuario.tipo;
     }
     public Usuario getUsuario(string id)
     {
         UsuarioBDO usuarioBDO = null;
         try
         {
             usuarioBDO = productLogic.getUsuario(id);
         }
         catch (Exception e)
         {
             string msg = e.Message;
             string reason = "getUsuario Exception";
             throw new FaultException<SocioFault>(new SocioFault(msg), reason);
         }
         if (usuarioBDO == null)
         {
             string msg = string.Format("No usuario found for id {0}", id);
             string reason = "getUsuario Empty Usuario";
         }
         Usuario usuario = new Usuario();
         translateUsuarioBDOtoUsuarioDTO(usuarioBDO, usuario);
         return usuario;
     }

     //COPIAS
     private void translateCopiaBDOtoCopiaDTO(CopiaBDO bdo, Copia dto)
     {
         dto.idcopia = bdo.idcopia;
         dto.noserie = bdo.noserie;
     }
     public int getNumeroDeCopiasDisponiblesParaPelicula(int id)
     {
         return productLogic.getNumeroDeCopiasDisponiblesParaPelicula(id);
     }
     public ArrayCopias getCopiasWithQuery(string query, string[] paramters, string[] values)
     {
         ArrayCopias ac = new ArrayCopias();
         ArrayList resultado = productLogic.getCopiasWithQuery(query, paramters, values);
         Copia[] copias = new Copia[resultado.Count];

         for (int i = 0; i < copias.Length; ++i)
         {
             Copia c = new Copia();
             translateCopiaBDOtoCopiaDTO((CopiaBDO)resultado[i], c);
             copias[i] = c;
         }

         ac.copias = copias;
         return ac;
     }


     //PELICULAS
     public void translatePeliculaBDOtoDTO(PeliculaBDO bdo, Pelicula dto)
     {
         dto.noserie = bdo.noserie;
         dto.titutlo = bdo.titutlo;
         dto.anodefilmacion = bdo.anodefilmacion;
         dto.directores = bdo.directores;
         dto.actoresprincipales = bdo.actoresprincipales;
         dto.idiomaoriginal = bdo.idiomaoriginal;
         dto.idiomasdisponibles = bdo.idiomasdisponibles;
         dto.genero = bdo.genero;
         dto.duracion = bdo.duracion;
         dto.tipo = bdo.tipo;
     }
     public void translatePeliculaDTOtoBDO(Pelicula dto, PeliculaBDO bdo)
     {
         bdo.noserie = dto.noserie;
         bdo.titutlo = dto.titutlo;
         bdo.anodefilmacion = dto.anodefilmacion;
         bdo.directores = dto.directores;
         bdo.actoresprincipales = dto.actoresprincipales;
         bdo.idiomaoriginal = dto.idiomaoriginal;
         bdo.idiomasdisponibles = dto.idiomasdisponibles;
         bdo.genero = dto.genero;
         bdo.duracion = dto.duracion;
         bdo.tipo = dto.tipo;
     }

     public ArrayPeliculas getPeliculasWithQuery(string query, string[] parameters, object[] values)
     {
         ArrayPeliculas apeliculas = new ArrayPeliculas();

         ArrayList resultado = productLogic.getPeliculasWithQuery(query, parameters, values);
         Pelicula[] p = new Pelicula[resultado.Count];

         //Se traducen todos los objetos para que puedan ser guardados
         for (int i = 0; i < resultado.Count; ++i)
         {
             Pelicula pelicula = new Pelicula();
             translatePeliculaBDOtoDTO((PeliculaBDO)resultado[i], pelicula);
             p[i] = pelicula;
         }

         //Se guardan los objetos en su contenedor correspondiente para ser enviados
         apeliculas.peliculas = p;
         return apeliculas;
     }

     //COSTOS
     private void translateCostoBDOtoDTO(CostoBDO bdo, Costo dto)
     {
         dto.tipo = bdo.tipo;
         dto.precio = bdo.precio;
         dto.tiemporenta = bdo.tiemporenta;
     }
     public ArrayCostos getCostosWithQuery(string query, string[] parameters, string[] values)
     {
         ArrayList resultados = productLogic.getCostosWithQuery(query, parameters, values);

         Costo[] costos = new Costo[resultados.Count];

         for (int i = 0; i < costos.Length; ++i)
         {
             Costo c = new Costo();
             translateCostoBDOtoDTO((CostoBDO)resultados[i], c);
             costos[i] = c;
         }

         ArrayCostos ac = new ArrayCostos();
         ac.costos = costos;
         return ac;

     }

     //BAJAS
     private void translateBajaBDOtoDTO(BajaBDO bdo, Baja dto)
     {
         dto.idbaja = bdo.idbaja;
         dto.idcopia = bdo.idcopia;
         dto.razon = bdo.razon;
     }
     public ArrayBajas getBajasWithQuery(string query, string[] parameters, string[] values)
     {
         //Se hace la conversión
         ArrayList resultado = productLogic.getBajasWithQuery(query, parameters, values);
         Baja[] bajas = new Baja[resultado.Count];

         for (int i = 0; i < bajas.Length; ++i)
         {
             Baja b = new Baja();
             translateBajaBDOtoDTO((BajaBDO)resultado[i], b);
             bajas[i] = b;
         }

         ArrayBajas ab = new ArrayBajas();
         ab.bajas = bajas;

         return ab;

     }

     //RESERVAS
     private void translateReservaBDOtoDTO(ReservaBDO bdo, Reserva dto)
     {
         dto.noserie = bdo.noserie;
         dto.socio = bdo.socio;
         dto.fechareserva = bdo.fechareserva;
         dto.idreserva = bdo.idreserva;
     }
     public ArrayReservas getReservasWithQuery(string query, string[] parameters, object[] values)
     {
         //Se hace la conversión 
         ArrayList resultado = productLogic.getReservasWithQuery(query, parameters, values);
         Reserva[] rs = new Reserva[resultado.Count];

         for (int i = 0; i < rs.Length; ++i)
         {
             Reserva r = new Reserva();
             translateReservaBDOtoDTO((ReservaBDO)resultado[i], r);
             rs[i] = r;
         }

         ArrayReservas ar = new ArrayReservas();
         ar.reservas = rs;
         return ar;
     }

     //RENTAS
     private void translateRentaBDOtoDTO(RentaBDO bdo, Renta dto)
     {
         dto.idcopia = bdo.idcopia;
         dto.idrenta = bdo.idrenta;
         dto.idsocio = bdo.idsocio;
         dto.noserie = bdo.noserie;
         dto.costo = bdo.costo;
         dto.estado = bdo.estado;
         dto.fecha = bdo.fechaentrega;
         dto.fechaentrega = bdo.fechaentrega;
     }

     public ArrayRentas getRentasWithQuery(string query, string[] parameters, object[] values)
     {
         ArrayList resultado = productLogic.getRentasWithQuery(query, parameters, values);
         Renta[] rs = new Renta[resultado.Count];

         for (int i = 0; i < rs.Length; ++i)
         {
             Renta r = new Renta();
             translateRentaBDOtoDTO((RentaBDO)resultado[i], r);
             rs[i] = r;
         }

         ArrayRentas ar = new ArrayRentas();
         ar.rentas = rs;
         return ar;

     }

     //PARA MULTAS
     private void translateMultaBDOtoDTO(MultaBDO bdo, Multa dto)
     {
         dto.idmulta = bdo.idmulta;
         dto.idrenta = bdo.idrenta;
         dto.multatotal = bdo.multatotal;
     }
     public ArrayMultas getMultasWithQuery(string query, string[] parameters, object[] values)
     {
         ArrayList resultado = productLogic.getMultasWithQuery(query, parameters, values);
         Multa[] ms = new Multa[resultado.Count];

         for (int i = 0; i < ms.Length; ++i)
         {
             Multa r = new Multa();
             translateMultaBDOtoDTO((MultaBDO)resultado[i], r);
             ms[i] = r;
         }

         ArrayMultas am = new ArrayMultas();
         am.multas = ms;
         return am;

     }

     public ArrayUsuarios getUsuarioWithQuery(string query, string[] parameters, object[] values)
     {
         ArrayList resultado = productLogic.getUsuarioWithQuery(query, parameters, values);
         Usuario[] usuarios = new Usuario[resultado.Count];

         for(int i = 0; i < usuarios.Length; ++i)
         {
             Usuario u = new Usuario();
             translateUsuarioBDOtoUsuarioDTO((UsuarioBDO)resultado[i], u);
             usuarios[i] = u;
         }

         ArrayUsuarios au = new ArrayUsuarios();
         au.usuarios = usuarios;
         return au;
     }

     //EJECUCIÓN
     public bool executeNonQuery(string query, string[] parameters, object[] values)
     {
         return productLogic.executeNonQuery(query, parameters, values);
     }


 }
}