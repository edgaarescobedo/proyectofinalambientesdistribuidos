﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class Reservas : Form
    {
        ProductServiceClient client = new ProductServiceClient();
        Form padre;

        public Reservas(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            llenarTablaReservas();
        }

        private void Reservas_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void llenarTablaReservas()
        {
            reservasTable.ClearSelection();

            //Limpiando la tabla
            reservasTable.Rows.Clear();

            reservasTable.AllowUserToAddRows = true;

            //Se obtienen las reservas
            ArrayReservas ar = client.getReservasWithQuery("SELECT * FROM RESERVAS WHERE SOCIO=@SOCIO;", new string[] { "@SOCIO" }, new string[] { StaticClass.socio + "" });

            //Se llena la tabla con las reservas obtenidos
            for (int i = 0; i < ar.reservas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(reservasTable);
                row.Cells[0].Value = ar.reservas[i].idreserva;
                row.Cells[1].Value = ar.reservas[i].fechareserva;
                
                //Se tiene que obtener el título de la película de la reserva
                ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE;", new string[]{"@NOSERIE"}, new string[]{ar.reservas[i].noserie+""});
                row.Cells[2].Value = ap.peliculas[0].titutlo;

                reservasTable.Rows.Add(row);
            }

            reservasTable.AllowUserToAddRows = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Se tien que checar que haya alguna reserva seleccionada
            if (reservasTable.SelectedCells.Count == 0)
            {
                MessageBox.Show("Debes de seleccionar una reserva para eliminarla");
                return;
            }

            DataGridViewRow row = reservasTable.Rows[reservasTable.SelectedCells[0].RowIndex];

            bool resultado = client.executeNonQuery("DELETE FROM RESERVAS WHERE IDRESERVA=@IDRESERVA", new string[] { "@IDRESERVA" }, new string[] { row.Cells[0].Value.ToString() });

            if (!resultado)
            {
                MessageBox.Show("Problema al realizar la operación con el servidor");
                return;
            }

            llenarTablaReservas();

        }
    }
}
