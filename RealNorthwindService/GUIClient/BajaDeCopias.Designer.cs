﻿namespace GUIClient
{
    partial class BajaDeCopias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bajasTable = new System.Windows.Forms.DataGridView();
            this.IDBAJA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDCOPIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.idbajaTextbox = new System.Windows.Forms.TextBox();
            this.actualizarRadioButton = new System.Windows.Forms.RadioButton();
            this.ingresarRadioButton = new System.Windows.Forms.RadioButton();
            this.idcopiaTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.razonTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.copiasTable = new System.Windows.Forms.DataGridView();
            this.NOSERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDCOPIA2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITULO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingresarinformacionButton = new System.Windows.Forms.Button();
            this.eliminarButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bajasTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copiasTable)).BeginInit();
            this.SuspendLayout();
            // 
            // bajasTable
            // 
            this.bajasTable.AllowUserToAddRows = false;
            this.bajasTable.AllowUserToDeleteRows = false;
            this.bajasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bajasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDBAJA,
            this.IDCOPIA});
            this.bajasTable.Location = new System.Drawing.Point(12, 12);
            this.bajasTable.MultiSelect = false;
            this.bajasTable.Name = "bajasTable";
            this.bajasTable.ReadOnly = true;
            this.bajasTable.Size = new System.Drawing.Size(246, 150);
            this.bajasTable.TabIndex = 0;
            this.bajasTable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bajasTable_KeyDown);
            this.bajasTable.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bajasTable_KeyUp);
            this.bajasTable.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bajasTable_MouseClick);
            // 
            // IDBAJA
            // 
            this.IDBAJA.HeaderText = "IDBAJA";
            this.IDBAJA.Name = "IDBAJA";
            this.IDBAJA.ReadOnly = true;
            // 
            // IDCOPIA
            // 
            this.IDCOPIA.HeaderText = "IDCOPIA";
            this.IDCOPIA.Name = "IDCOPIA";
            this.IDCOPIA.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(80, 242);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ID Baja";
            // 
            // idbajaTextbox
            // 
            this.idbajaTextbox.Location = new System.Drawing.Point(125, 239);
            this.idbajaTextbox.Name = "idbajaTextbox";
            this.idbajaTextbox.ReadOnly = true;
            this.idbajaTextbox.Size = new System.Drawing.Size(56, 20);
            this.idbajaTextbox.TabIndex = 2;
            // 
            // actualizarRadioButton
            // 
            this.actualizarRadioButton.AutoSize = true;
            this.actualizarRadioButton.Checked = true;
            this.actualizarRadioButton.Location = new System.Drawing.Point(12, 168);
            this.actualizarRadioButton.Name = "actualizarRadioButton";
            this.actualizarRadioButton.Size = new System.Drawing.Size(129, 17);
            this.actualizarRadioButton.TabIndex = 3;
            this.actualizarRadioButton.TabStop = true;
            this.actualizarRadioButton.Text = "Actualizar Información";
            this.actualizarRadioButton.UseVisualStyleBackColor = true;
            this.actualizarRadioButton.CheckedChanged += new System.EventHandler(this.actualizarRadioButton_CheckedChanged);
            // 
            // ingresarRadioButton
            // 
            this.ingresarRadioButton.AutoSize = true;
            this.ingresarRadioButton.Location = new System.Drawing.Point(12, 191);
            this.ingresarRadioButton.Name = "ingresarRadioButton";
            this.ingresarRadioButton.Size = new System.Drawing.Size(156, 17);
            this.ingresarRadioButton.TabIndex = 4;
            this.ingresarRadioButton.Text = "Ingresar Nueva Información";
            this.ingresarRadioButton.UseVisualStyleBackColor = true;
            this.ingresarRadioButton.CheckedChanged += new System.EventHandler(this.ingresarRadioButton_CheckedChanged);
            // 
            // idcopiaTextbox
            // 
            this.idcopiaTextbox.Location = new System.Drawing.Point(125, 265);
            this.idcopiaTextbox.Name = "idcopiaTextbox";
            this.idcopiaTextbox.Size = new System.Drawing.Size(56, 20);
            this.idcopiaTextbox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "ID Copia";
            // 
            // razonTextbox
            // 
            this.razonTextbox.Location = new System.Drawing.Point(125, 291);
            this.razonTextbox.Name = "razonTextbox";
            this.razonTextbox.Size = new System.Drawing.Size(442, 20);
            this.razonTextbox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 294);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Razón";
            // 
            // copiasTable
            // 
            this.copiasTable.AllowUserToAddRows = false;
            this.copiasTable.AllowUserToDeleteRows = false;
            this.copiasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.copiasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NOSERIE,
            this.IDCOPIA2,
            this.TITULO});
            this.copiasTable.Location = new System.Drawing.Point(283, 12);
            this.copiasTable.MultiSelect = false;
            this.copiasTable.Name = "copiasTable";
            this.copiasTable.ReadOnly = true;
            this.copiasTable.Size = new System.Drawing.Size(423, 150);
            this.copiasTable.TabIndex = 9;
            // 
            // NOSERIE
            // 
            this.NOSERIE.HeaderText = "NOSERIE";
            this.NOSERIE.Name = "NOSERIE";
            this.NOSERIE.ReadOnly = true;
            // 
            // IDCOPIA2
            // 
            this.IDCOPIA2.HeaderText = "IDCOPIA";
            this.IDCOPIA2.Name = "IDCOPIA2";
            this.IDCOPIA2.ReadOnly = true;
            // 
            // TITULO
            // 
            this.TITULO.HeaderText = "TITULO";
            this.TITULO.Name = "TITULO";
            this.TITULO.ReadOnly = true;
            // 
            // ingresarinformacionButton
            // 
            this.ingresarinformacionButton.Location = new System.Drawing.Point(271, 354);
            this.ingresarinformacionButton.Name = "ingresarinformacionButton";
            this.ingresarinformacionButton.Size = new System.Drawing.Size(150, 23);
            this.ingresarinformacionButton.TabIndex = 10;
            this.ingresarinformacionButton.Text = "Ingresar Información";
            this.ingresarinformacionButton.UseVisualStyleBackColor = true;
            this.ingresarinformacionButton.Click += new System.EventHandler(this.ingresarinformacionButton_Click);
            // 
            // eliminarButton
            // 
            this.eliminarButton.Enabled = false;
            this.eliminarButton.Location = new System.Drawing.Point(271, 383);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Size = new System.Drawing.Size(150, 23);
            this.eliminarButton.TabIndex = 11;
            this.eliminarButton.Text = "Eliminar";
            this.eliminarButton.UseVisualStyleBackColor = true;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // BajaDeCopias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 437);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.ingresarinformacionButton);
            this.Controls.Add(this.copiasTable);
            this.Controls.Add(this.razonTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.idcopiaTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ingresarRadioButton);
            this.Controls.Add(this.actualizarRadioButton);
            this.Controls.Add(this.idbajaTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bajasTable);
            this.Name = "BajaDeCopias";
            this.Text = "BajaDeCopias";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BajaDeCopias_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.bajasTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copiasTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView bajasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDBAJA;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCOPIA;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox idbajaTextbox;
        private System.Windows.Forms.RadioButton actualizarRadioButton;
        private System.Windows.Forms.RadioButton ingresarRadioButton;
        private System.Windows.Forms.TextBox idcopiaTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox razonTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView copiasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOSERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCOPIA2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO;
        private System.Windows.Forms.Button ingresarinformacionButton;
        private System.Windows.Forms.Button eliminarButton;
    }
}