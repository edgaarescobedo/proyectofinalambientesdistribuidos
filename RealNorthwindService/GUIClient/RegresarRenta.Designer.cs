﻿namespace GUIClient
{
    partial class RegresarRenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rentasTable = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.adeudoTextbox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.IDRENTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDSOCIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHAENTREG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COSTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.rentasTable)).BeginInit();
            this.SuspendLayout();
            // 
            // rentasTable
            // 
            this.rentasTable.AllowUserToAddRows = false;
            this.rentasTable.AllowUserToDeleteRows = false;
            this.rentasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rentasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDRENTA,
            this.IDSOCIO,
            this.FECHAENTREG,
            this.COSTO});
            this.rentasTable.Location = new System.Drawing.Point(12, 12);
            this.rentasTable.MultiSelect = false;
            this.rentasTable.Name = "rentasTable";
            this.rentasTable.ReadOnly = true;
            this.rentasTable.Size = new System.Drawing.Size(609, 150);
            this.rentasTable.TabIndex = 0;
            this.rentasTable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rentasTable_KeyDown);
            this.rentasTable.KeyUp += new System.Windows.Forms.KeyEventHandler(this.rentasTable_KeyUp);
            this.rentasTable.MouseClick += new System.Windows.Forms.MouseEventHandler(this.rentasTable_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(241, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Monto de Adeudo";
            // 
            // adeudoTextbox
            // 
            this.adeudoTextbox.Location = new System.Drawing.Point(339, 203);
            this.adeudoTextbox.Name = "adeudoTextbox";
            this.adeudoTextbox.ReadOnly = true;
            this.adeudoTextbox.Size = new System.Drawing.Size(100, 20);
            this.adeudoTextbox.TabIndex = 2;
            this.adeudoTextbox.Text = "0.0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(262, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Regresar y/o Cobrar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IDRENTA
            // 
            this.IDRENTA.HeaderText = "IDRENTA";
            this.IDRENTA.Name = "IDRENTA";
            this.IDRENTA.ReadOnly = true;
            // 
            // IDSOCIO
            // 
            this.IDSOCIO.HeaderText = "IDSOCIO";
            this.IDSOCIO.Name = "IDSOCIO";
            this.IDSOCIO.ReadOnly = true;
            // 
            // FECHAENTREG
            // 
            this.FECHAENTREG.HeaderText = "FECHA DE ENTREGA";
            this.FECHAENTREG.Name = "FECHAENTREG";
            this.FECHAENTREG.ReadOnly = true;
            // 
            // COSTO
            // 
            this.COSTO.HeaderText = "COSTO";
            this.COSTO.Name = "COSTO";
            this.COSTO.ReadOnly = true;
            // 
            // RegresarRenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 328);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.adeudoTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rentasTable);
            this.Name = "RegresarRenta";
            this.Text = "RegresarRenta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegresarRenta_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.rentasTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView rentasTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox adeudoTextbox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDRENTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDSOCIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHAENTREG;
        private System.Windows.Forms.DataGridViewTextBoxColumn COSTO;
    }
}