﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class RegistroUsuarios : Form
    {

        Form padre;
        ProductServiceClient client = new ProductServiceClient();

        public RegistroUsuarios(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            llenarTabla();
        }

        private void llenarTabla()
        {
            ArrayUsuarios au = client.getUsuarioWithQuery("SELECT * FROM USUARIOS", new string[0], new object[0]);

            usuariosTable.AllowUserToAddRows = true;
            usuariosTable.Rows.Clear();

            for (int i = 0; i < au.usuarios.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(usuariosTable);

                row.Cells[0].Value = au.usuarios[i].id;
                row.Cells[1].Value = au.usuarios[i].tipo;
                row.Cells[2].Value = au.usuarios[i].password;

                usuariosTable.Rows.Add(row);
            }

            usuariosTable.AllowUserToAddRows = false;

        }

        private void actualizarRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            idTexbox.ReadOnly = true;
            usuariosTable.Enabled = true;

        }

        private void ingresarRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            idTexbox.ReadOnly = false;
            usuariosTable.Enabled = false;
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            if(usuariosTable.SelectedCells.Count == 0)
            {
                return;
            }

            string[] p = {"@ID"};
            object[] v = {usuariosTable.SelectedCells[0].OwningRow.Cells[0].Value};
            bool resultado = client.executeNonQuery("DELETE FROM USUARIOS WHERE ID=@ID;", p, v);

            if (!resultado)
            {
                MessageBox.Show("Problema al realizar la operación en el servidor");
                return;
            }

            llenarTabla();
            limpiarCampos();
        }

        private void limpiarCampos()
        {
            idTexbox.Text = "";
            tipoCombobox.Text = "";
            passwordTextbox.Text = "";
        }

        private void enterButton_Click(object sender, EventArgs e)
        {
            if (idTexbox.Text == "" || passwordTextbox.Text == "" || tipoCombobox.Text == "")
            {
                MessageBox.Show("Por favor llena todos los campos");
                return;
            }
            
            string[] p = { "@ID", "@TIPO", "@PASSWORD" };
            object[] v = { idTexbox.Text, tipoCombobox.Text, passwordTextbox.Text };
            bool resultado = false;

            if (actualizarRadiobutton.Checked)
            {
                resultado = client.executeNonQuery("UPDATE USUARIOS SET TIPO=@TIPO, PASSWORD=@PASSWORD WHERE ID=@ID;", p, v);
            }
            else
            {
                string[] p1 = {"@ID"};
                object[] v1 = {idTexbox.Text};
                //Se busca si no hay ya un usuario con ese id
                ArrayUsuarios au = client.getUsuarioWithQuery("SELECT * FROM USUARIOS WHERE ID=@ID;", p1, v1);
                if (au.usuarios.Length > 0)
                {
                    MessageBox.Show("Ya existe un usuario con este id");
                    return;
                }

                resultado = client.executeNonQuery("INSERT INTO USUARIOS (ID, TIPO, PASSWORD) VALUES (@ID, @TIPO, @PASSWORD);", p, v);
            }

            if (!resultado)
            {
                MessageBox.Show("Problema al realizar la operación en el servidor");
                return;
            }

            limpiarCampos();
            llenarTabla();
        }

        private void RegistroUsuarios_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void tablaSeleccionada()
        {
            DataGridViewRow row = usuariosTable.SelectedCells[0].OwningRow;

            idTexbox.Text = row.Cells[0].Value.ToString();
            tipoCombobox.Text = row.Cells[1].Value.ToString();
            passwordTextbox.Text = row.Cells[2].Value.ToString();
        }

        private void usuariosTable_MouseClick(object sender, MouseEventArgs e)
        {
            tablaSeleccionada();
        }

        private void usuariosTable_KeyDown(object sender, KeyEventArgs e)
        {
            tablaSeleccionada();
        }

        private void usuariosTable_KeyUp(object sender, KeyEventArgs e)
        {
            tablaSeleccionada();
        }
    }
}
