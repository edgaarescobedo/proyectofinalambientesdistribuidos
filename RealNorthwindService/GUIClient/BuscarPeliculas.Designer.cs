﻿namespace GUIClient
{
    partial class BuscarPeliculas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tituloRadioButton = new System.Windows.Forms.RadioButton();
            this.generoRadioButton = new System.Windows.Forms.RadioButton();
            this.anoRadioButton = new System.Windows.Forms.RadioButton();
            this.actorRadioButton = new System.Windows.Forms.RadioButton();
            this.directorRadioButton = new System.Windows.Forms.RadioButton();
            this.textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buscarButton = new System.Windows.Forms.Button();
            this.peliculasDatagrid = new System.Windows.Forms.DataGridView();
            this.NOSERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Titulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Actores = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AnoDeFilmacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Directores = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Genero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reservarButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.copiasDisponiblesTextbox = new System.Windows.Forms.TextBox();
            this.precioTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tipoTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.reservaDatetime = new System.Windows.Forms.DateTimePicker();
            this.omitirCheckbox = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tepuedeinteresarTable = new System.Windows.Forms.DataGridView();
            this.TITULO2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.refrescarButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.peliculasDatagrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tepuedeinteresarTable)).BeginInit();
            this.SuspendLayout();
            // 
            // tituloRadioButton
            // 
            this.tituloRadioButton.AutoSize = true;
            this.tituloRadioButton.Checked = true;
            this.tituloRadioButton.Location = new System.Drawing.Point(275, 44);
            this.tituloRadioButton.Name = "tituloRadioButton";
            this.tituloRadioButton.Size = new System.Drawing.Size(53, 17);
            this.tituloRadioButton.TabIndex = 10;
            this.tituloRadioButton.TabStop = true;
            this.tituloRadioButton.Text = "Título";
            this.tituloRadioButton.UseVisualStyleBackColor = true;
            // 
            // generoRadioButton
            // 
            this.generoRadioButton.AutoSize = true;
            this.generoRadioButton.Location = new System.Drawing.Point(360, 44);
            this.generoRadioButton.Name = "generoRadioButton";
            this.generoRadioButton.Size = new System.Drawing.Size(60, 17);
            this.generoRadioButton.TabIndex = 11;
            this.generoRadioButton.Text = "Género";
            this.generoRadioButton.UseVisualStyleBackColor = true;
            // 
            // anoRadioButton
            // 
            this.anoRadioButton.AutoSize = true;
            this.anoRadioButton.Location = new System.Drawing.Point(457, 44);
            this.anoRadioButton.Name = "anoRadioButton";
            this.anoRadioButton.Size = new System.Drawing.Size(44, 17);
            this.anoRadioButton.TabIndex = 12;
            this.anoRadioButton.Text = "Año";
            this.anoRadioButton.UseVisualStyleBackColor = true;
            // 
            // actorRadioButton
            // 
            this.actorRadioButton.AutoSize = true;
            this.actorRadioButton.Location = new System.Drawing.Point(548, 44);
            this.actorRadioButton.Name = "actorRadioButton";
            this.actorRadioButton.Size = new System.Drawing.Size(50, 17);
            this.actorRadioButton.TabIndex = 13;
            this.actorRadioButton.Text = "Actor";
            this.actorRadioButton.UseVisualStyleBackColor = true;
            // 
            // directorRadioButton
            // 
            this.directorRadioButton.AutoSize = true;
            this.directorRadioButton.Location = new System.Drawing.Point(639, 44);
            this.directorRadioButton.Name = "directorRadioButton";
            this.directorRadioButton.Size = new System.Drawing.Size(62, 17);
            this.directorRadioButton.TabIndex = 14;
            this.directorRadioButton.Text = "Director";
            this.directorRadioButton.UseVisualStyleBackColor = true;
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(301, 81);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(266, 20);
            this.textBox.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(434, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Buscar por...";
            // 
            // buscarButton
            // 
            this.buscarButton.Location = new System.Drawing.Point(594, 79);
            this.buscarButton.Name = "buscarButton";
            this.buscarButton.Size = new System.Drawing.Size(75, 23);
            this.buscarButton.TabIndex = 17;
            this.buscarButton.Text = "Buscar";
            this.buscarButton.UseVisualStyleBackColor = true;
            this.buscarButton.Click += new System.EventHandler(this.buscarButton_Click);
            // 
            // peliculasDatagrid
            // 
            this.peliculasDatagrid.AllowUserToAddRows = false;
            this.peliculasDatagrid.AllowUserToDeleteRows = false;
            this.peliculasDatagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.peliculasDatagrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NOSERIE,
            this.Titulo,
            this.Actores,
            this.AnoDeFilmacion,
            this.Directores,
            this.Genero,
            this.TIPO});
            this.peliculasDatagrid.Location = new System.Drawing.Point(19, 137);
            this.peliculasDatagrid.MultiSelect = false;
            this.peliculasDatagrid.Name = "peliculasDatagrid";
            this.peliculasDatagrid.ReadOnly = true;
            this.peliculasDatagrid.Size = new System.Drawing.Size(893, 150);
            this.peliculasDatagrid.TabIndex = 18;
            this.peliculasDatagrid.Click += new System.EventHandler(this.peliculasDatagrid_Click);
            this.peliculasDatagrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.peliculasDatagrid_KeyDown);
            this.peliculasDatagrid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.peliculasDatagrid_KeyUp);
            // 
            // NOSERIE
            // 
            this.NOSERIE.HeaderText = "NOSERIE";
            this.NOSERIE.Name = "NOSERIE";
            this.NOSERIE.ReadOnly = true;
            this.NOSERIE.Visible = false;
            // 
            // Titulo
            // 
            this.Titulo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Titulo.HeaderText = "Título";
            this.Titulo.Name = "Titulo";
            this.Titulo.ReadOnly = true;
            this.Titulo.Width = 60;
            // 
            // Actores
            // 
            this.Actores.HeaderText = "Actores";
            this.Actores.Name = "Actores";
            this.Actores.ReadOnly = true;
            // 
            // AnoDeFilmacion
            // 
            this.AnoDeFilmacion.HeaderText = "Año de Filmación";
            this.AnoDeFilmacion.Name = "AnoDeFilmacion";
            this.AnoDeFilmacion.ReadOnly = true;
            // 
            // Directores
            // 
            this.Directores.HeaderText = "Directores";
            this.Directores.Name = "Directores";
            this.Directores.ReadOnly = true;
            // 
            // Genero
            // 
            this.Genero.HeaderText = "Genero";
            this.Genero.Name = "Genero";
            this.Genero.ReadOnly = true;
            // 
            // TIPO
            // 
            this.TIPO.HeaderText = "TIPO";
            this.TIPO.Name = "TIPO";
            this.TIPO.ReadOnly = true;
            this.TIPO.Visible = false;
            // 
            // reservarButton
            // 
            this.reservarButton.Enabled = false;
            this.reservarButton.Location = new System.Drawing.Point(372, 427);
            this.reservarButton.Name = "reservarButton";
            this.reservarButton.Size = new System.Drawing.Size(222, 23);
            this.reservarButton.TabIndex = 20;
            this.reservarButton.Text = "Reservar Película seleccionada";
            this.reservarButton.UseVisualStyleBackColor = true;
            this.reservarButton.Click += new System.EventHandler(this.reservarButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 310);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Copias Disponibles:";
            // 
            // copiasDisponiblesTextbox
            // 
            this.copiasDisponiblesTextbox.Location = new System.Drawing.Point(125, 307);
            this.copiasDisponiblesTextbox.Name = "copiasDisponiblesTextbox";
            this.copiasDisponiblesTextbox.ReadOnly = true;
            this.copiasDisponiblesTextbox.Size = new System.Drawing.Size(23, 20);
            this.copiasDisponiblesTextbox.TabIndex = 22;
            // 
            // precioTextbox
            // 
            this.precioTextbox.Location = new System.Drawing.Point(242, 307);
            this.precioTextbox.Name = "precioTextbox";
            this.precioTextbox.ReadOnly = true;
            this.precioTextbox.Size = new System.Drawing.Size(86, 20);
            this.precioTextbox.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(196, 310);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Precio:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(357, 310);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Tipo:";
            // 
            // tipoTextbox
            // 
            this.tipoTextbox.Location = new System.Drawing.Point(394, 307);
            this.tipoTextbox.Name = "tipoTextbox";
            this.tipoTextbox.ReadOnly = true;
            this.tipoTextbox.Size = new System.Drawing.Size(86, 20);
            this.tipoTextbox.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(336, 382);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Reservar para el día: ";
            // 
            // reservaDatetime
            // 
            this.reservaDatetime.Location = new System.Drawing.Point(452, 376);
            this.reservaDatetime.MinDate = new System.DateTime(2018, 12, 6, 0, 0, 0, 0);
            this.reservaDatetime.Name = "reservaDatetime";
            this.reservaDatetime.Size = new System.Drawing.Size(200, 20);
            this.reservaDatetime.TabIndex = 28;
            // 
            // omitirCheckbox
            // 
            this.omitirCheckbox.AutoSize = true;
            this.omitirCheckbox.Location = new System.Drawing.Point(19, 85);
            this.omitirCheckbox.Name = "omitirCheckbox";
            this.omitirCheckbox.Size = new System.Drawing.Size(157, 17);
            this.omitirCheckbox.TabIndex = 29;
            this.omitirCheckbox.Text = "Omitir Películas ya rentadas";
            this.omitirCheckbox.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 481);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Te puede interesar:";
            // 
            // tepuedeinteresarTable
            // 
            this.tepuedeinteresarTable.AllowUserToAddRows = false;
            this.tepuedeinteresarTable.AllowUserToDeleteRows = false;
            this.tepuedeinteresarTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tepuedeinteresarTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TITULO2,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.tepuedeinteresarTable.Location = new System.Drawing.Point(23, 507);
            this.tepuedeinteresarTable.MultiSelect = false;
            this.tepuedeinteresarTable.Name = "tepuedeinteresarTable";
            this.tepuedeinteresarTable.ReadOnly = true;
            this.tepuedeinteresarTable.Size = new System.Drawing.Size(889, 97);
            this.tepuedeinteresarTable.TabIndex = 30;
            // 
            // TITULO2
            // 
            this.TITULO2.HeaderText = "TITUTLO";
            this.TITULO2.Name = "TITULO2";
            this.TITULO2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "AÑO DE FILMACION";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ACTORES";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "DIRECTORES";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // refrescarButton
            // 
            this.refrescarButton.Location = new System.Drawing.Point(837, 481);
            this.refrescarButton.Name = "refrescarButton";
            this.refrescarButton.Size = new System.Drawing.Size(75, 23);
            this.refrescarButton.TabIndex = 32;
            this.refrescarButton.Text = "Refrescar";
            this.refrescarButton.UseVisualStyleBackColor = true;
            this.refrescarButton.Click += new System.EventHandler(this.refrescarButton_Click);
            // 
            // BuscarPeliculas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 629);
            this.Controls.Add(this.refrescarButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tepuedeinteresarTable);
            this.Controls.Add(this.omitirCheckbox);
            this.Controls.Add(this.reservaDatetime);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tipoTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.precioTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.copiasDisponiblesTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.reservarButton);
            this.Controls.Add(this.peliculasDatagrid);
            this.Controls.Add(this.buscarButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.directorRadioButton);
            this.Controls.Add(this.actorRadioButton);
            this.Controls.Add(this.anoRadioButton);
            this.Controls.Add(this.generoRadioButton);
            this.Controls.Add(this.tituloRadioButton);
            this.Name = "BuscarPeliculas";
            this.Text = "BuscarPeliculas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BuscarPeliculas_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.peliculasDatagrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tepuedeinteresarTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton tituloRadioButton;
        private System.Windows.Forms.RadioButton generoRadioButton;
        private System.Windows.Forms.RadioButton anoRadioButton;
        private System.Windows.Forms.RadioButton actorRadioButton;
        private System.Windows.Forms.RadioButton directorRadioButton;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buscarButton;
        private System.Windows.Forms.DataGridView peliculasDatagrid;
        private System.Windows.Forms.Button reservarButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox copiasDisponiblesTextbox;
        private System.Windows.Forms.TextBox precioTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tipoTextbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOSERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Titulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Actores;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnoDeFilmacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Directores;
        private System.Windows.Forms.DataGridViewTextBoxColumn Genero;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker reservaDatetime;
        private System.Windows.Forms.CheckBox omitirCheckbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView tepuedeinteresarTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button refrescarButton;
    }
}