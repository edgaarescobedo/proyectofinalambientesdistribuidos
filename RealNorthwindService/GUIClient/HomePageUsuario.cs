﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIClient
{
    public partial class HomePageUsuario : Form
    {
        public HomePageUsuario(String tipo)
        {
            InitializeComponent();

            if (tipo == "VENDEDOR")
            {
                adminSocios.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                button5.Enabled = false;
                button6.Enabled = false;
            }
        }

        private void HomePageUsuario_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void adminSocios_Click(object sender, EventArgs e)
        {
            new RegistroSocios(this).Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new AdministrarPeliculas(this).Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new BajaDeCopias(this).Show();
        }

        private void puntoDeVenta_Click(object sender, EventArgs e)
        {
            new RentaPeliculas(this).Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new RegresarRenta(this).Show();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Inicio().Show();
            this.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new RegistroCopias(this).Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new RegistroUsuarios(this).Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new Estadisticas(this).Show();
        }
    }
}
