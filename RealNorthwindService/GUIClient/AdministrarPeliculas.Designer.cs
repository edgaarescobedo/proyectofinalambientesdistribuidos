﻿namespace GUIClient
{
    partial class AdministrarPeliculas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.peliculasTable = new System.Windows.Forms.DataGridView();
            this.NOSERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITULO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIRECTORES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACTORES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.noserieTextbox = new System.Windows.Forms.TextBox();
            this.tituloTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.anodefilmacionSpinbox = new System.Windows.Forms.NumericUpDown();
            this.directoresTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.actoresTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.idiomaoriginalTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.idiomasdisponiblesTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.generoTextbox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.duracionSpinbox = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tipoCombobox = new System.Windows.Forms.ComboBox();
            this.actualizarRadiobutton = new System.Windows.Forms.RadioButton();
            this.ingresarRadiobutton = new System.Windows.Forms.RadioButton();
            this.ingresarinformacionButton = new System.Windows.Forms.Button();
            this.eliminarButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.peliculasTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.anodefilmacionSpinbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.duracionSpinbox)).BeginInit();
            this.SuspendLayout();
            // 
            // peliculasTable
            // 
            this.peliculasTable.AllowUserToAddRows = false;
            this.peliculasTable.AllowUserToDeleteRows = false;
            this.peliculasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.peliculasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NOSERIE,
            this.TITULO,
            this.DIRECTORES,
            this.ACTORES});
            this.peliculasTable.Location = new System.Drawing.Point(12, 12);
            this.peliculasTable.Name = "peliculasTable";
            this.peliculasTable.ReadOnly = true;
            this.peliculasTable.Size = new System.Drawing.Size(641, 150);
            this.peliculasTable.TabIndex = 0;
            this.peliculasTable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.peliculasTable_KeyDown);
            this.peliculasTable.KeyUp += new System.Windows.Forms.KeyEventHandler(this.peliculasTable_KeyUp);
            this.peliculasTable.MouseClick += new System.Windows.Forms.MouseEventHandler(this.peliculasTable_MouseClick);
            // 
            // NOSERIE
            // 
            this.NOSERIE.HeaderText = "NOSERIE";
            this.NOSERIE.Name = "NOSERIE";
            this.NOSERIE.ReadOnly = true;
            // 
            // TITULO
            // 
            this.TITULO.HeaderText = "TITUTLO";
            this.TITULO.Name = "TITULO";
            this.TITULO.ReadOnly = true;
            // 
            // DIRECTORES
            // 
            this.DIRECTORES.HeaderText = "DIRECTORES";
            this.DIRECTORES.Name = "DIRECTORES";
            this.DIRECTORES.ReadOnly = true;
            // 
            // ACTORES
            // 
            this.ACTORES.HeaderText = "ACTORES";
            this.ACTORES.Name = "ACTORES";
            this.ACTORES.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(215, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "No. Serie";
            // 
            // noserieTextbox
            // 
            this.noserieTextbox.Location = new System.Drawing.Point(272, 224);
            this.noserieTextbox.Name = "noserieTextbox";
            this.noserieTextbox.ReadOnly = true;
            this.noserieTextbox.Size = new System.Drawing.Size(121, 20);
            this.noserieTextbox.TabIndex = 2;
            // 
            // tituloTextbox
            // 
            this.tituloTextbox.Location = new System.Drawing.Point(272, 250);
            this.tituloTextbox.Name = "tituloTextbox";
            this.tituloTextbox.Size = new System.Drawing.Size(121, 20);
            this.tituloTextbox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(233, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Titulo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 279);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Año de Filmación";
            // 
            // anodefilmacionSpinbox
            // 
            this.anodefilmacionSpinbox.Location = new System.Drawing.Point(272, 277);
            this.anodefilmacionSpinbox.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.anodefilmacionSpinbox.Minimum = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            this.anodefilmacionSpinbox.Name = "anodefilmacionSpinbox";
            this.anodefilmacionSpinbox.Size = new System.Drawing.Size(121, 20);
            this.anodefilmacionSpinbox.TabIndex = 7;
            this.anodefilmacionSpinbox.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            // 
            // directoresTextbox
            // 
            this.directoresTextbox.Location = new System.Drawing.Point(272, 303);
            this.directoresTextbox.Name = "directoresTextbox";
            this.directoresTextbox.Size = new System.Drawing.Size(121, 20);
            this.directoresTextbox.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(211, 306);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Directores";
            // 
            // actoresTextbox
            // 
            this.actoresTextbox.Location = new System.Drawing.Point(272, 329);
            this.actoresTextbox.Name = "actoresTextbox";
            this.actoresTextbox.Size = new System.Drawing.Size(121, 20);
            this.actoresTextbox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 332);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Actores Principales";
            // 
            // idiomaoriginalTextbox
            // 
            this.idiomaoriginalTextbox.Location = new System.Drawing.Point(272, 355);
            this.idiomaoriginalTextbox.Name = "idiomaoriginalTextbox";
            this.idiomaoriginalTextbox.Size = new System.Drawing.Size(121, 20);
            this.idiomaoriginalTextbox.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(190, 358);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Idioma Original";
            // 
            // idiomasdisponiblesTextbox
            // 
            this.idiomasdisponiblesTextbox.Location = new System.Drawing.Point(272, 381);
            this.idiomasdisponiblesTextbox.Name = "idiomasdisponiblesTextbox";
            this.idiomasdisponiblesTextbox.Size = new System.Drawing.Size(121, 20);
            this.idiomasdisponiblesTextbox.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(166, 384);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Idiomas Disponibles";
            // 
            // generoTextbox
            // 
            this.generoTextbox.Location = new System.Drawing.Point(272, 406);
            this.generoTextbox.Name = "generoTextbox";
            this.generoTextbox.Size = new System.Drawing.Size(121, 20);
            this.generoTextbox.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(224, 409);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Género";
            // 
            // duracionSpinbox
            // 
            this.duracionSpinbox.DecimalPlaces = 2;
            this.duracionSpinbox.Location = new System.Drawing.Point(272, 432);
            this.duracionSpinbox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.duracionSpinbox.Name = "duracionSpinbox";
            this.duracionSpinbox.Size = new System.Drawing.Size(121, 20);
            this.duracionSpinbox.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(216, 434);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Duración";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(238, 461);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Tipo";
            // 
            // tipoCombobox
            // 
            this.tipoCombobox.FormattingEnabled = true;
            this.tipoCombobox.Items.AddRange(new object[] {
            "ESTRENO",
            "NORMAL",
            "PROMOCION"});
            this.tipoCombobox.Location = new System.Drawing.Point(271, 458);
            this.tipoCombobox.Name = "tipoCombobox";
            this.tipoCombobox.Size = new System.Drawing.Size(121, 21);
            this.tipoCombobox.TabIndex = 22;
            // 
            // actualizarRadiobutton
            // 
            this.actualizarRadiobutton.AutoSize = true;
            this.actualizarRadiobutton.Checked = true;
            this.actualizarRadiobutton.Location = new System.Drawing.Point(12, 181);
            this.actualizarRadiobutton.Name = "actualizarRadiobutton";
            this.actualizarRadiobutton.Size = new System.Drawing.Size(129, 17);
            this.actualizarRadiobutton.TabIndex = 23;
            this.actualizarRadiobutton.TabStop = true;
            this.actualizarRadiobutton.Text = "Actualizar Información";
            this.actualizarRadiobutton.UseVisualStyleBackColor = true;
            this.actualizarRadiobutton.CheckedChanged += new System.EventHandler(this.actualizarRadiobutton_CheckedChanged);
            // 
            // ingresarRadiobutton
            // 
            this.ingresarRadiobutton.AutoSize = true;
            this.ingresarRadiobutton.Location = new System.Drawing.Point(12, 204);
            this.ingresarRadiobutton.Name = "ingresarRadiobutton";
            this.ingresarRadiobutton.Size = new System.Drawing.Size(105, 17);
            this.ingresarRadiobutton.TabIndex = 24;
            this.ingresarRadiobutton.Text = "Ingresar Película";
            this.ingresarRadiobutton.UseVisualStyleBackColor = true;
            this.ingresarRadiobutton.CheckedChanged += new System.EventHandler(this.ingresarRadiobutton_CheckedChanged);
            // 
            // ingresarinformacionButton
            // 
            this.ingresarinformacionButton.Location = new System.Drawing.Point(466, 302);
            this.ingresarinformacionButton.Name = "ingresarinformacionButton";
            this.ingresarinformacionButton.Size = new System.Drawing.Size(131, 23);
            this.ingresarinformacionButton.TabIndex = 25;
            this.ingresarinformacionButton.Text = "Ingresar Información";
            this.ingresarinformacionButton.UseVisualStyleBackColor = true;
            this.ingresarinformacionButton.Click += new System.EventHandler(this.ingresarinformacionButton_Click);
            // 
            // eliminarButton
            // 
            this.eliminarButton.Location = new System.Drawing.Point(466, 355);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Size = new System.Drawing.Size(131, 23);
            this.eliminarButton.TabIndex = 26;
            this.eliminarButton.Text = "Eliminar";
            this.eliminarButton.UseVisualStyleBackColor = true;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // AdministrarPeliculas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 519);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.ingresarinformacionButton);
            this.Controls.Add(this.ingresarRadiobutton);
            this.Controls.Add(this.actualizarRadiobutton);
            this.Controls.Add(this.tipoCombobox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.duracionSpinbox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.generoTextbox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.idiomasdisponiblesTextbox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.idiomaoriginalTextbox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.actoresTextbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.directoresTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.anodefilmacionSpinbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tituloTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.noserieTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.peliculasTable);
            this.Name = "AdministrarPeliculas";
            this.Text = "AdministrarPeliculas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdministrarPeliculas_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.peliculasTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.anodefilmacionSpinbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.duracionSpinbox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView peliculasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOSERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIRECTORES;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACTORES;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox noserieTextbox;
        private System.Windows.Forms.TextBox tituloTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown anodefilmacionSpinbox;
        private System.Windows.Forms.TextBox directoresTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox actoresTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox idiomaoriginalTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox idiomasdisponiblesTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox generoTextbox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown duracionSpinbox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox tipoCombobox;
        private System.Windows.Forms.RadioButton actualizarRadiobutton;
        private System.Windows.Forms.RadioButton ingresarRadiobutton;
        private System.Windows.Forms.Button ingresarinformacionButton;
        private System.Windows.Forms.Button eliminarButton;
    }
}