﻿namespace GUIClient
{
    partial class RegistroCopias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.copiasTable = new System.Windows.Forms.DataGridView();
            this.NOSERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDCOPIA2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITULO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idcopiaTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.noserieTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ingresarButton = new System.Windows.Forms.Button();
            this.peliculasTable = new System.Windows.Forms.DataGridView();
            this.NOSERIE1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITULO1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eliminarButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.copiasTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peliculasTable)).BeginInit();
            this.SuspendLayout();
            // 
            // copiasTable
            // 
            this.copiasTable.AllowUserToAddRows = false;
            this.copiasTable.AllowUserToDeleteRows = false;
            this.copiasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.copiasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NOSERIE,
            this.IDCOPIA2,
            this.TITULO});
            this.copiasTable.Location = new System.Drawing.Point(12, 12);
            this.copiasTable.MultiSelect = false;
            this.copiasTable.Name = "copiasTable";
            this.copiasTable.ReadOnly = true;
            this.copiasTable.Size = new System.Drawing.Size(355, 150);
            this.copiasTable.TabIndex = 10;
            // 
            // NOSERIE
            // 
            this.NOSERIE.HeaderText = "NOSERIE";
            this.NOSERIE.Name = "NOSERIE";
            this.NOSERIE.ReadOnly = true;
            // 
            // IDCOPIA2
            // 
            this.IDCOPIA2.HeaderText = "IDCOPIA";
            this.IDCOPIA2.Name = "IDCOPIA2";
            this.IDCOPIA2.ReadOnly = true;
            // 
            // TITULO
            // 
            this.TITULO.HeaderText = "TITULO";
            this.TITULO.Name = "TITULO";
            this.TITULO.ReadOnly = true;
            // 
            // idcopiaTextbox
            // 
            this.idcopiaTextbox.Location = new System.Drawing.Point(325, 184);
            this.idcopiaTextbox.Name = "idcopiaTextbox";
            this.idcopiaTextbox.ReadOnly = true;
            this.idcopiaTextbox.Size = new System.Drawing.Size(56, 20);
            this.idcopiaTextbox.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "ID Copia";
            // 
            // noserieTextbox
            // 
            this.noserieTextbox.Location = new System.Drawing.Point(325, 210);
            this.noserieTextbox.Name = "noserieTextbox";
            this.noserieTextbox.Size = new System.Drawing.Size(56, 20);
            this.noserieTextbox.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(274, 213);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "No. Serie";
            // 
            // ingresarButton
            // 
            this.ingresarButton.Location = new System.Drawing.Point(292, 247);
            this.ingresarButton.Name = "ingresarButton";
            this.ingresarButton.Size = new System.Drawing.Size(75, 23);
            this.ingresarButton.TabIndex = 15;
            this.ingresarButton.Text = "Ingresar";
            this.ingresarButton.UseVisualStyleBackColor = true;
            this.ingresarButton.Click += new System.EventHandler(this.ingresarButton_Click);
            // 
            // peliculasTable
            // 
            this.peliculasTable.AllowUserToAddRows = false;
            this.peliculasTable.AllowUserToDeleteRows = false;
            this.peliculasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.peliculasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NOSERIE1,
            this.TITULO1});
            this.peliculasTable.Location = new System.Drawing.Point(386, 12);
            this.peliculasTable.Name = "peliculasTable";
            this.peliculasTable.ReadOnly = true;
            this.peliculasTable.Size = new System.Drawing.Size(240, 150);
            this.peliculasTable.TabIndex = 16;
            // 
            // NOSERIE1
            // 
            this.NOSERIE1.HeaderText = "NOSERIE";
            this.NOSERIE1.Name = "NOSERIE1";
            this.NOSERIE1.ReadOnly = true;
            // 
            // TITULO1
            // 
            this.TITULO1.HeaderText = "TITULO";
            this.TITULO1.Name = "TITULO1";
            this.TITULO1.ReadOnly = true;
            // 
            // eliminarButton
            // 
            this.eliminarButton.Location = new System.Drawing.Point(12, 168);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Size = new System.Drawing.Size(75, 23);
            this.eliminarButton.TabIndex = 17;
            this.eliminarButton.Text = "Eliminar";
            this.eliminarButton.UseVisualStyleBackColor = true;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // RegistroCopias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 298);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.peliculasTable);
            this.Controls.Add(this.ingresarButton);
            this.Controls.Add(this.noserieTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.idcopiaTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.copiasTable);
            this.Name = "RegistroCopias";
            this.Text = "RegistroCopias";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegistroCopias_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.copiasTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peliculasTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView copiasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOSERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCOPIA2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO;
        private System.Windows.Forms.TextBox idcopiaTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox noserieTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ingresarButton;
        private System.Windows.Forms.DataGridView peliculasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOSERIE1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO1;
        private System.Windows.Forms.Button eliminarButton;
    }
}