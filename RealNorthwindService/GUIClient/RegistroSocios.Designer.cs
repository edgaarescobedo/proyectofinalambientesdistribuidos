﻿namespace GUIClient
{
    partial class RegistroSocios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.actualizarRadioButton = new System.Windows.Forms.RadioButton();
            this.agregarRadioButton = new System.Windows.Forms.RadioButton();
            this.asdasdasda = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ingresarButton = new System.Windows.Forms.Button();
            this.socioTextbox = new System.Windows.Forms.TextBox();
            this.nombreTextbox = new System.Windows.Forms.TextBox();
            this.documentoCombobox = new System.Windows.Forms.ComboBox();
            this.nacimientoDatepicker = new System.Windows.Forms.DateTimePicker();
            this.domicilioTextbox = new System.Windows.Forms.TextBox();
            this.inicioDatepicker = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMBRE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NACIMIENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.passwordTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.eliminarButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // actualizarRadioButton
            // 
            this.actualizarRadioButton.AutoSize = true;
            this.actualizarRadioButton.Checked = true;
            this.actualizarRadioButton.Location = new System.Drawing.Point(12, 142);
            this.actualizarRadioButton.Name = "actualizarRadioButton";
            this.actualizarRadioButton.Size = new System.Drawing.Size(129, 17);
            this.actualizarRadioButton.TabIndex = 1;
            this.actualizarRadioButton.TabStop = true;
            this.actualizarRadioButton.Text = "Actualizar Información";
            this.actualizarRadioButton.UseVisualStyleBackColor = true;
            this.actualizarRadioButton.CheckedChanged += new System.EventHandler(this.actualizarRadioButton_CheckedChanged);
            // 
            // agregarRadioButton
            // 
            this.agregarRadioButton.AutoSize = true;
            this.agregarRadioButton.Location = new System.Drawing.Point(12, 165);
            this.agregarRadioButton.Name = "agregarRadioButton";
            this.agregarRadioButton.Size = new System.Drawing.Size(92, 17);
            this.agregarRadioButton.TabIndex = 2;
            this.agregarRadioButton.Text = "Agregar Socio";
            this.agregarRadioButton.UseVisualStyleBackColor = true;
            this.agregarRadioButton.CheckedChanged += new System.EventHandler(this.agregarRadioButton_CheckedChanged);
            // 
            // asdasdasda
            // 
            this.asdasdasda.AutoSize = true;
            this.asdasdasda.Location = new System.Drawing.Point(94, 222);
            this.asdasdasda.Name = "asdasdasda";
            this.asdasdasda.Size = new System.Drawing.Size(89, 13);
            this.asdasdasda.TabIndex = 3;
            this.asdasdasda.Text = "Número de Socio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 251);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 279);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Documento de identificación";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(77, 305);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Fecha de nacimiento";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(134, 337);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Domicilio";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 370);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Fecha de inicio como socio";
            // 
            // ingresarButton
            // 
            this.ingresarButton.Location = new System.Drawing.Point(189, 450);
            this.ingresarButton.Name = "ingresarButton";
            this.ingresarButton.Size = new System.Drawing.Size(134, 23);
            this.ingresarButton.TabIndex = 9;
            this.ingresarButton.Text = "Ingresar Información";
            this.ingresarButton.UseVisualStyleBackColor = true;
            this.ingresarButton.Click += new System.EventHandler(this.ingresarButton_Click);
            // 
            // socioTextbox
            // 
            this.socioTextbox.Location = new System.Drawing.Point(189, 217);
            this.socioTextbox.Name = "socioTextbox";
            this.socioTextbox.Size = new System.Drawing.Size(200, 20);
            this.socioTextbox.TabIndex = 10;
            // 
            // nombreTextbox
            // 
            this.nombreTextbox.Location = new System.Drawing.Point(189, 244);
            this.nombreTextbox.Name = "nombreTextbox";
            this.nombreTextbox.Size = new System.Drawing.Size(200, 20);
            this.nombreTextbox.TabIndex = 12;
            // 
            // documentoCombobox
            // 
            this.documentoCombobox.FormattingEnabled = true;
            this.documentoCombobox.Items.AddRange(new object[] {
            "INE",
            "LICENCIA",
            "ACTA"});
            this.documentoCombobox.Location = new System.Drawing.Point(189, 271);
            this.documentoCombobox.Name = "documentoCombobox";
            this.documentoCombobox.Size = new System.Drawing.Size(200, 21);
            this.documentoCombobox.TabIndex = 13;
            // 
            // nacimientoDatepicker
            // 
            this.nacimientoDatepicker.Location = new System.Drawing.Point(189, 299);
            this.nacimientoDatepicker.Name = "nacimientoDatepicker";
            this.nacimientoDatepicker.Size = new System.Drawing.Size(200, 20);
            this.nacimientoDatepicker.TabIndex = 14;
            // 
            // domicilioTextbox
            // 
            this.domicilioTextbox.Location = new System.Drawing.Point(189, 330);
            this.domicilioTextbox.Name = "domicilioTextbox";
            this.domicilioTextbox.Size = new System.Drawing.Size(200, 20);
            this.domicilioTextbox.TabIndex = 15;
            // 
            // inicioDatepicker
            // 
            this.inicioDatepicker.Location = new System.Drawing.Point(189, 363);
            this.inicioDatepicker.Name = "inicioDatepicker";
            this.inicioDatepicker.Size = new System.Drawing.Size(200, 20);
            this.inicioDatepicker.TabIndex = 16;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.NOMBRE,
            this.NACIMIENTO});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(497, 114);
            this.dataGridView1.TabIndex = 17;
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            this.dataGridView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyUp);
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // NOMBRE
            // 
            this.NOMBRE.HeaderText = "NOMBRE";
            this.NOMBRE.Name = "NOMBRE";
            this.NOMBRE.ReadOnly = true;
            this.NOMBRE.Width = 250;
            // 
            // NACIMIENTO
            // 
            this.NACIMIENTO.HeaderText = "NACIMIENTO";
            this.NACIMIENTO.Name = "NACIMIENTO";
            this.NACIMIENTO.ReadOnly = true;
            // 
            // numeroTextbox
            // 
            this.numeroTextbox.Location = new System.Drawing.Point(189, 389);
            this.numeroTextbox.Name = "numeroTextbox";
            this.numeroTextbox.Size = new System.Drawing.Size(200, 20);
            this.numeroTextbox.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(139, 392);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Numero";
            // 
            // passwordTextbox
            // 
            this.passwordTextbox.Location = new System.Drawing.Point(189, 415);
            this.passwordTextbox.Name = "passwordTextbox";
            this.passwordTextbox.Size = new System.Drawing.Size(200, 20);
            this.passwordTextbox.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(130, 418);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Password";
            // 
            // eliminarButton
            // 
            this.eliminarButton.Location = new System.Drawing.Point(434, 132);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Size = new System.Drawing.Size(75, 23);
            this.eliminarButton.TabIndex = 22;
            this.eliminarButton.Text = "Eliminar";
            this.eliminarButton.UseVisualStyleBackColor = true;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // RegistroSocios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 485);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.passwordTextbox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numeroTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.inicioDatepicker);
            this.Controls.Add(this.domicilioTextbox);
            this.Controls.Add(this.nacimientoDatepicker);
            this.Controls.Add(this.documentoCombobox);
            this.Controls.Add(this.nombreTextbox);
            this.Controls.Add(this.socioTextbox);
            this.Controls.Add(this.ingresarButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.asdasdasda);
            this.Controls.Add(this.agregarRadioButton);
            this.Controls.Add(this.actualizarRadioButton);
            this.Name = "RegistroSocios";
            this.Text = "RegistroSocios";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegistroSocios_FormClosing);
            this.Load += new System.EventHandler(this.RegistroSocios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton actualizarRadioButton;
        private System.Windows.Forms.RadioButton agregarRadioButton;
        private System.Windows.Forms.Label asdasdasda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button ingresarButton;
        private System.Windows.Forms.TextBox socioTextbox;
        private System.Windows.Forms.TextBox nombreTextbox;
        private System.Windows.Forms.ComboBox documentoCombobox;
        private System.Windows.Forms.DateTimePicker nacimientoDatepicker;
        private System.Windows.Forms.TextBox domicilioTextbox;
        private System.Windows.Forms.DateTimePicker inicioDatepicker;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMBRE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NACIMIENTO;
        private System.Windows.Forms.TextBox numeroTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox passwordTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button eliminarButton;
    }
}