﻿namespace GUIClient
{
    partial class HomePageSocio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buscarPeliculasButton = new System.Windows.Forms.Button();
            this.verAdeudosButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reservasButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buscarPeliculasButton
            // 
            this.buscarPeliculasButton.Location = new System.Drawing.Point(43, 89);
            this.buscarPeliculasButton.Name = "buscarPeliculasButton";
            this.buscarPeliculasButton.Size = new System.Drawing.Size(191, 23);
            this.buscarPeliculasButton.TabIndex = 0;
            this.buscarPeliculasButton.Text = "Buscar Y Reservar Películas";
            this.buscarPeliculasButton.UseVisualStyleBackColor = true;
            this.buscarPeliculasButton.Click += new System.EventHandler(this.buscarPeliculasButton_Click);
            // 
            // verAdeudosButton
            // 
            this.verAdeudosButton.Location = new System.Drawing.Point(43, 169);
            this.verAdeudosButton.Name = "verAdeudosButton";
            this.verAdeudosButton.Size = new System.Drawing.Size(191, 23);
            this.verAdeudosButton.TabIndex = 1;
            this.verAdeudosButton.Text = "Ver Adeudos";
            this.verAdeudosButton.UseVisualStyleBackColor = true;
            this.verAdeudosButton.Click += new System.EventHandler(this.verAdeudosButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sesiónToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sesiónToolStripMenuItem
            // 
            this.sesiónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.sesiónToolStripMenuItem.Name = "sesiónToolStripMenuItem";
            this.sesiónToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.sesiónToolStripMenuItem.Text = "Acciones";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.salirToolStripMenuItem.Text = "Cerrar Sesión";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // reservasButton
            // 
            this.reservasButton.Location = new System.Drawing.Point(43, 130);
            this.reservasButton.Name = "reservasButton";
            this.reservasButton.Size = new System.Drawing.Size(191, 23);
            this.reservasButton.TabIndex = 3;
            this.reservasButton.Text = "Ver Reservas";
            this.reservasButton.UseVisualStyleBackColor = true;
            this.reservasButton.Click += new System.EventHandler(this.reservasButton_Click);
            // 
            // HomePageSocio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.reservasButton);
            this.Controls.Add(this.verAdeudosButton);
            this.Controls.Add(this.buscarPeliculasButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "HomePageSocio";
            this.Text = "HomePageSocio";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HomePageSocio_FormClosing);
            this.Load += new System.EventHandler(this.HomePageSocio_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buscarPeliculasButton;
        private System.Windows.Forms.Button verAdeudosButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sesiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.Button reservasButton;
    }
}