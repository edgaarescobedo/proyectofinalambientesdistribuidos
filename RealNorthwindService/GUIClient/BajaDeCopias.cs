﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class BajaDeCopias : Form
    {
        ProductServiceClient client = new ProductServiceClient();
        Form padre;

        public BajaDeCopias(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            llenarCopiasTable();
            llenarBajasTable();
        }

        private void BajaDeCopias_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void llenarCopiasTable()
        {
            copiasTable.ClearSelection();

            //Limpiando la tabla
            copiasTable.Rows.Clear();

            copiasTable.AllowUserToAddRows = true;

            //Se obtienen las copias
            ArrayCopias ac = client.getCopiasWithQuery("SELECT * FROM COPIAS", new string[0], new string[0]);

            //Se llena la tabla con los socios obtenidos
            for (int i = 0; i < ac.copias.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(copiasTable);
                row.Cells[0].Value = ac.copias[i].noserie;
                row.Cells[1].Value = ac.copias[i].idcopia;
                
                //Se tiene que pedir el título para poder llenar los títulos
                ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE", new string[] { "@NOSERIE" }, new string[] { ac.copias[i].noserie+"" });
                row.Cells[2].Value = ap.peliculas[0].titutlo;

                copiasTable.Rows.Add(row);
            }

            copiasTable.AllowUserToAddRows = false;
        }

        private void llenarBajasTable()
        {
            bajasTable.ClearSelection();

            //Limpiando la tabla
            bajasTable.Rows.Clear();

            bajasTable.AllowUserToAddRows = true;

            //Se obtienen las bajas
            ArrayBajas ab = client.getBajasWithQuery("SELECT * FROM BAJAS", new string[0], new string[0]);

            //Se llena la tabla con los socios obtenidos
            for (int i = 0; i < ab.bajas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(bajasTable);
                row.Cells[0].Value = ab.bajas[i].idbaja;
                row.Cells[1].Value = ab.bajas[i].idcopia;
                bajasTable.Rows.Add(row);
            }

            bajasTable.AllowUserToAddRows = false;
        }

        private void actualizarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            bajasTable.Enabled = true;
            limpiarCampos();
        }

        private void bajaSeleccionada()
        {
            //Se obtiene la baja seleccionada
            DataGridViewRow row = bajasTable.Rows[bajasTable.SelectedCells[0].RowIndex];

            ArrayBajas ab = client.getBajasWithQuery("SELECT * FROM BAJAS WHERE IDBAJA=@IDBAJA", new string[] { "@IDBAJA" }, new string[] { row.Cells[0].Value.ToString() });
            idbajaTextbox.Text = ab.bajas[0].idbaja + "";
            idcopiaTextbox.Text = ab.bajas[0].idcopia + "";
            razonTextbox.Text = ab.bajas[0].razon;

            eliminarButton.Enabled = true;

        }

        private void bajasTable_MouseClick(object sender, MouseEventArgs e)
        {
            bajaSeleccionada();
        }

        private void bajasTable_KeyDown(object sender, KeyEventArgs e)
        {
            bajaSeleccionada();
        }

        private void bajasTable_KeyUp(object sender, KeyEventArgs e)
        {
            bajaSeleccionada();
        }

        private void limpiarCampos()
        {
            idbajaTextbox.Text = "";
            idcopiaTextbox.Text = "";
            razonTextbox.Text = "";
        }

        private void ingresarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            eliminarButton.Enabled = false;
            bajasTable.Enabled = false;
            limpiarCampos();

            //Se obtiene nuevo indice
            ArrayBajas ab = client.getBajasWithQuery("SELECT * FROM BAJAS WHERE IDBAJA = (SELECT MAX(IDBAJA) FROM BAJAS);", new string[0], new string[0]);
            int idbaja = ab.bajas[0].idbaja + 1;
            idbajaTextbox.Text = idbaja + "";


        }

        private void ingresarinformacionButton_Click(object sender, EventArgs e)
        {
            //Verificando que no haya nada vacío
            if (idbajaTextbox.Text == "" || idcopiaTextbox.Text == "" || razonTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingresa la información completa para continuar");
                return;
            }

            string[] parameters = { "@IDBAJA", "@IDCOPIA", "@RAZON" };
            string[] values = { idbajaTextbox.Text, idcopiaTextbox.Text, razonTextbox.Text };

            bool exito = false;

            if (actualizarRadioButton.Checked)
            {
                exito = client.executeNonQuery("UPDATE BAJAS SET IDCOPIA=@IDCOPIA, RAZON=@RAZON WHERE IDBAJA=@IDBAJA;", parameters, values);
            }
            else if (ingresarRadioButton.Checked)
            {
                //Se tiene que verificar que no exista ya la copia dentro del sistema
                ArrayBajas ab = client.getBajasWithQuery("SELECT * FROM BAJAS WHERE IDCOPIA=@IDCOPIA", new string[] { "@IDCOPIA" }, new string[] { idcopiaTextbox.Text });

                if (ab.bajas.Length > 0)
                {
                    MessageBox.Show("No puedes duplicar copias en bajas, por favor ingresa un id de copia diferente");
                    return;
                }

                exito = client.executeNonQuery("INSERT INTO BAJAS (IDBAJA, IDCOPIA, RAZON) VALUES (@IDBAJA, @IDCOPIA, @RAZON);", parameters, values);
            }

            if (!exito)
            {
                MessageBox.Show("Problema al realizar la operación en el servidor");
            }
            else
            {
                actualizarRadioButton.Checked = true;
                llenarBajasTable();
                llenarCopiasTable();
            }
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            //Se obtiene la baja seleccionada
            DataGridViewRow row = bajasTable.Rows[bajasTable.SelectedCells[0].RowIndex];

            string[] p = { "@IDBAJA" };
            string[] v = { row.Cells[0].Value.ToString() };

            bool exito = client.executeNonQuery("DELETE FROM BAJAS WHERE IDBAJA=@IDBAJA", p, v);

            if (!exito)
            {
                MessageBox.Show("Problema al realizar la operación en el servidor");
                return;
            }

            llenarBajasTable();
            llenarCopiasTable();

        }
    }
}
