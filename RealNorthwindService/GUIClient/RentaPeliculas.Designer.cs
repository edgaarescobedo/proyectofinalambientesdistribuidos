﻿namespace GUIClient
{
    partial class RentaPeliculas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.socioTextbox = new System.Windows.Forms.TextBox();
            this.noserieTexbox = new System.Windows.Forms.TextBox();
            this.ingresarpeliculaButton = new System.Windows.Forms.Button();
            this.buscarSocioButton = new System.Windows.Forms.Button();
            this.peliculasarentarTable = new System.Windows.Forms.DataGridView();
            this.NOSERIE1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITULO1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COSTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDCOPIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.tepuedeinteresarTable = new System.Windows.Forms.DataGridView();
            this.TITULO2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANODEFILMACION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACTORES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DIRECTORES = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.totalTextbox = new System.Windows.Forms.TextBox();
            this.cobrarButton = new System.Windows.Forms.Button();
            this.nombresocioTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.todaslaspeliculasTable = new System.Windows.Forms.DataGridView();
            this.NOSERIE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITULO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buscarButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox = new System.Windows.Forms.TextBox();
            this.directorRadioButton = new System.Windows.Forms.RadioButton();
            this.actorRadioButton = new System.Windows.Forms.RadioButton();
            this.anoRadioButton = new System.Windows.Forms.RadioButton();
            this.generoRadioButton = new System.Windows.Forms.RadioButton();
            this.tituloRadioButton = new System.Windows.Forms.RadioButton();
            this.quitarpeliculaButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.peliculasarentarTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tepuedeinteresarTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.todaslaspeliculasTable)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Socio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID Película";
            // 
            // socioTextbox
            // 
            this.socioTextbox.Location = new System.Drawing.Point(92, 31);
            this.socioTextbox.Name = "socioTextbox";
            this.socioTextbox.Size = new System.Drawing.Size(100, 20);
            this.socioTextbox.TabIndex = 2;
            this.socioTextbox.TextChanged += new System.EventHandler(this.socioTextbox_TextChanged);
            // 
            // noserieTexbox
            // 
            this.noserieTexbox.Location = new System.Drawing.Point(92, 102);
            this.noserieTexbox.Name = "noserieTexbox";
            this.noserieTexbox.Size = new System.Drawing.Size(100, 20);
            this.noserieTexbox.TabIndex = 3;
            // 
            // ingresarpeliculaButton
            // 
            this.ingresarpeliculaButton.Location = new System.Drawing.Point(212, 102);
            this.ingresarpeliculaButton.Name = "ingresarpeliculaButton";
            this.ingresarpeliculaButton.Size = new System.Drawing.Size(75, 23);
            this.ingresarpeliculaButton.TabIndex = 4;
            this.ingresarpeliculaButton.Text = "Ingresar";
            this.ingresarpeliculaButton.UseVisualStyleBackColor = true;
            this.ingresarpeliculaButton.Click += new System.EventHandler(this.ingresarpeliculaButton_Click);
            // 
            // buscarSocioButton
            // 
            this.buscarSocioButton.Location = new System.Drawing.Point(212, 29);
            this.buscarSocioButton.Name = "buscarSocioButton";
            this.buscarSocioButton.Size = new System.Drawing.Size(75, 23);
            this.buscarSocioButton.TabIndex = 5;
            this.buscarSocioButton.Text = "Buscar";
            this.buscarSocioButton.UseVisualStyleBackColor = true;
            this.buscarSocioButton.Click += new System.EventHandler(this.buscarSocioButton_Click);
            // 
            // peliculasarentarTable
            // 
            this.peliculasarentarTable.AllowUserToAddRows = false;
            this.peliculasarentarTable.AllowUserToDeleteRows = false;
            this.peliculasarentarTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.peliculasarentarTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NOSERIE1,
            this.TITULO1,
            this.COSTO,
            this.IDCOPIA});
            this.peliculasarentarTable.Location = new System.Drawing.Point(29, 171);
            this.peliculasarentarTable.MultiSelect = false;
            this.peliculasarentarTable.Name = "peliculasarentarTable";
            this.peliculasarentarTable.ReadOnly = true;
            this.peliculasarentarTable.Size = new System.Drawing.Size(357, 97);
            this.peliculasarentarTable.TabIndex = 6;
            // 
            // NOSERIE1
            // 
            this.NOSERIE1.HeaderText = "NOSERIE";
            this.NOSERIE1.Name = "NOSERIE1";
            this.NOSERIE1.ReadOnly = true;
            this.NOSERIE1.Width = 50;
            // 
            // TITULO1
            // 
            this.TITULO1.HeaderText = "TITULO";
            this.TITULO1.Name = "TITULO1";
            this.TITULO1.ReadOnly = true;
            // 
            // COSTO
            // 
            this.COSTO.HeaderText = "COSTO";
            this.COSTO.Name = "COSTO";
            this.COSTO.ReadOnly = true;
            this.COSTO.Width = 50;
            // 
            // IDCOPIA
            // 
            this.IDCOPIA.HeaderText = "IDCOPIA";
            this.IDCOPIA.Name = "IDCOPIA";
            this.IDCOPIA.ReadOnly = true;
            this.IDCOPIA.Width = 50;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Películas a rentar";
            // 
            // tepuedeinteresarTable
            // 
            this.tepuedeinteresarTable.AllowUserToAddRows = false;
            this.tepuedeinteresarTable.AllowUserToDeleteRows = false;
            this.tepuedeinteresarTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tepuedeinteresarTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TITULO2,
            this.ANODEFILMACION,
            this.ACTORES,
            this.DIRECTORES});
            this.tepuedeinteresarTable.Location = new System.Drawing.Point(15, 423);
            this.tepuedeinteresarTable.MultiSelect = false;
            this.tepuedeinteresarTable.Name = "tepuedeinteresarTable";
            this.tepuedeinteresarTable.ReadOnly = true;
            this.tepuedeinteresarTable.Size = new System.Drawing.Size(906, 97);
            this.tepuedeinteresarTable.TabIndex = 8;
            // 
            // TITULO2
            // 
            this.TITULO2.HeaderText = "TITUTLO";
            this.TITULO2.Name = "TITULO2";
            this.TITULO2.ReadOnly = true;
            // 
            // ANODEFILMACION
            // 
            this.ANODEFILMACION.HeaderText = "AÑO DE FILMACION";
            this.ANODEFILMACION.Name = "ANODEFILMACION";
            this.ANODEFILMACION.ReadOnly = true;
            // 
            // ACTORES
            // 
            this.ACTORES.HeaderText = "ACTORES";
            this.ACTORES.Name = "ACTORES";
            this.ACTORES.ReadOnly = true;
            // 
            // DIRECTORES
            // 
            this.DIRECTORES.HeaderText = "DIRECTORES";
            this.DIRECTORES.Name = "DIRECTORES";
            this.DIRECTORES.ReadOnly = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 397);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Te puede interesar:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(95, 288);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Total a cobrar:";
            // 
            // totalTextbox
            // 
            this.totalTextbox.Location = new System.Drawing.Point(177, 285);
            this.totalTextbox.Name = "totalTextbox";
            this.totalTextbox.ReadOnly = true;
            this.totalTextbox.Size = new System.Drawing.Size(100, 20);
            this.totalTextbox.TabIndex = 11;
            // 
            // cobrarButton
            // 
            this.cobrarButton.Location = new System.Drawing.Point(151, 321);
            this.cobrarButton.Name = "cobrarButton";
            this.cobrarButton.Size = new System.Drawing.Size(75, 23);
            this.cobrarButton.TabIndex = 12;
            this.cobrarButton.Text = "Cobrar";
            this.cobrarButton.UseVisualStyleBackColor = true;
            this.cobrarButton.Click += new System.EventHandler(this.cobrarButton_Click);
            // 
            // nombresocioTextbox
            // 
            this.nombresocioTextbox.Location = new System.Drawing.Point(354, 31);
            this.nombresocioTextbox.Name = "nombresocioTextbox";
            this.nombresocioTextbox.ReadOnly = true;
            this.nombresocioTextbox.Size = new System.Drawing.Size(405, 20);
            this.nombresocioTextbox.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(304, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Nombre";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(514, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Todas las películas:";
            // 
            // todaslaspeliculasTable
            // 
            this.todaslaspeliculasTable.AllowUserToAddRows = false;
            this.todaslaspeliculasTable.AllowUserToDeleteRows = false;
            this.todaslaspeliculasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.todaslaspeliculasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NOSERIE,
            this.TITULO});
            this.todaslaspeliculasTable.Location = new System.Drawing.Point(517, 229);
            this.todaslaspeliculasTable.MultiSelect = false;
            this.todaslaspeliculasTable.Name = "todaslaspeliculasTable";
            this.todaslaspeliculasTable.ReadOnly = true;
            this.todaslaspeliculasTable.Size = new System.Drawing.Size(357, 97);
            this.todaslaspeliculasTable.TabIndex = 15;
            // 
            // NOSERIE
            // 
            this.NOSERIE.HeaderText = "NOSERIE";
            this.NOSERIE.Name = "NOSERIE";
            this.NOSERIE.ReadOnly = true;
            // 
            // TITULO
            // 
            this.TITULO.HeaderText = "TITULO";
            this.TITULO.Name = "TITULO";
            this.TITULO.ReadOnly = true;
            this.TITULO.Width = 200;
            // 
            // buscarButton
            // 
            this.buscarButton.Location = new System.Drawing.Point(810, 169);
            this.buscarButton.Name = "buscarButton";
            this.buscarButton.Size = new System.Drawing.Size(75, 23);
            this.buscarButton.TabIndex = 25;
            this.buscarButton.Text = "Buscar";
            this.buscarButton.UseVisualStyleBackColor = true;
            this.buscarButton.Click += new System.EventHandler(this.buscarButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(650, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Buscar por...";
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(517, 171);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(266, 20);
            this.textBox.TabIndex = 23;
            // 
            // directorRadioButton
            // 
            this.directorRadioButton.AutoSize = true;
            this.directorRadioButton.Location = new System.Drawing.Point(748, 134);
            this.directorRadioButton.Name = "directorRadioButton";
            this.directorRadioButton.Size = new System.Drawing.Size(62, 17);
            this.directorRadioButton.TabIndex = 22;
            this.directorRadioButton.Text = "Director";
            this.directorRadioButton.UseVisualStyleBackColor = true;
            // 
            // actorRadioButton
            // 
            this.actorRadioButton.AutoSize = true;
            this.actorRadioButton.Location = new System.Drawing.Point(692, 134);
            this.actorRadioButton.Name = "actorRadioButton";
            this.actorRadioButton.Size = new System.Drawing.Size(50, 17);
            this.actorRadioButton.TabIndex = 21;
            this.actorRadioButton.Text = "Actor";
            this.actorRadioButton.UseVisualStyleBackColor = true;
            // 
            // anoRadioButton
            // 
            this.anoRadioButton.AutoSize = true;
            this.anoRadioButton.Location = new System.Drawing.Point(642, 134);
            this.anoRadioButton.Name = "anoRadioButton";
            this.anoRadioButton.Size = new System.Drawing.Size(44, 17);
            this.anoRadioButton.TabIndex = 20;
            this.anoRadioButton.Text = "Año";
            this.anoRadioButton.UseVisualStyleBackColor = true;
            // 
            // generoRadioButton
            // 
            this.generoRadioButton.AutoSize = true;
            this.generoRadioButton.Location = new System.Drawing.Point(576, 134);
            this.generoRadioButton.Name = "generoRadioButton";
            this.generoRadioButton.Size = new System.Drawing.Size(60, 17);
            this.generoRadioButton.TabIndex = 19;
            this.generoRadioButton.Text = "Género";
            this.generoRadioButton.UseVisualStyleBackColor = true;
            // 
            // tituloRadioButton
            // 
            this.tituloRadioButton.AutoSize = true;
            this.tituloRadioButton.Checked = true;
            this.tituloRadioButton.Location = new System.Drawing.Point(517, 134);
            this.tituloRadioButton.Name = "tituloRadioButton";
            this.tituloRadioButton.Size = new System.Drawing.Size(53, 17);
            this.tituloRadioButton.TabIndex = 18;
            this.tituloRadioButton.TabStop = true;
            this.tituloRadioButton.Text = "Título";
            this.tituloRadioButton.UseVisualStyleBackColor = true;
            // 
            // quitarpeliculaButton
            // 
            this.quitarpeliculaButton.Location = new System.Drawing.Point(392, 200);
            this.quitarpeliculaButton.Name = "quitarpeliculaButton";
            this.quitarpeliculaButton.Size = new System.Drawing.Size(75, 23);
            this.quitarpeliculaButton.TabIndex = 26;
            this.quitarpeliculaButton.Text = "Quitar";
            this.quitarpeliculaButton.UseVisualStyleBackColor = true;
            this.quitarpeliculaButton.Click += new System.EventHandler(this.quitarpeliculaButton_Click);
            // 
            // RentaPeliculas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 536);
            this.Controls.Add(this.quitarpeliculaButton);
            this.Controls.Add(this.buscarButton);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.directorRadioButton);
            this.Controls.Add(this.actorRadioButton);
            this.Controls.Add(this.anoRadioButton);
            this.Controls.Add(this.generoRadioButton);
            this.Controls.Add(this.tituloRadioButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.todaslaspeliculasTable);
            this.Controls.Add(this.nombresocioTextbox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cobrarButton);
            this.Controls.Add(this.totalTextbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tepuedeinteresarTable);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.peliculasarentarTable);
            this.Controls.Add(this.buscarSocioButton);
            this.Controls.Add(this.ingresarpeliculaButton);
            this.Controls.Add(this.noserieTexbox);
            this.Controls.Add(this.socioTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "RentaPeliculas";
            this.Text = "RentaPeliculas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RentaPeliculas_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.peliculasarentarTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tepuedeinteresarTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.todaslaspeliculasTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox socioTextbox;
        private System.Windows.Forms.TextBox noserieTexbox;
        private System.Windows.Forms.Button ingresarpeliculaButton;
        private System.Windows.Forms.Button buscarSocioButton;
        private System.Windows.Forms.DataGridView peliculasarentarTable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView tepuedeinteresarTable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox totalTextbox;
        private System.Windows.Forms.Button cobrarButton;
        private System.Windows.Forms.TextBox nombresocioTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView todaslaspeliculasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOSERIE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANODEFILMACION;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACTORES;
        private System.Windows.Forms.DataGridViewTextBoxColumn DIRECTORES;
        private System.Windows.Forms.Button buscarButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.RadioButton directorRadioButton;
        private System.Windows.Forms.RadioButton actorRadioButton;
        private System.Windows.Forms.RadioButton anoRadioButton;
        private System.Windows.Forms.RadioButton generoRadioButton;
        private System.Windows.Forms.RadioButton tituloRadioButton;
        private System.Windows.Forms.Button quitarpeliculaButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOSERIE1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO1;
        private System.Windows.Forms.DataGridViewTextBoxColumn COSTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCOPIA;
    }
}