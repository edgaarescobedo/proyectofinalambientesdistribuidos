﻿namespace GUIClient
{
    partial class Reservas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reservasTable = new System.Windows.Forms.DataGridView();
            this.IDRESERVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHARESERVA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TITULO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.reservasTable)).BeginInit();
            this.SuspendLayout();
            // 
            // reservasTable
            // 
            this.reservasTable.AllowUserToAddRows = false;
            this.reservasTable.AllowUserToDeleteRows = false;
            this.reservasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reservasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDRESERVA,
            this.FECHARESERVA,
            this.TITULO});
            this.reservasTable.Location = new System.Drawing.Point(12, 12);
            this.reservasTable.MultiSelect = false;
            this.reservasTable.Name = "reservasTable";
            this.reservasTable.ReadOnly = true;
            this.reservasTable.Size = new System.Drawing.Size(493, 213);
            this.reservasTable.TabIndex = 0;
            // 
            // IDRESERVA
            // 
            this.IDRESERVA.HeaderText = "IDRESERVA";
            this.IDRESERVA.Name = "IDRESERVA";
            this.IDRESERVA.ReadOnly = true;
            // 
            // FECHARESERVA
            // 
            this.FECHARESERVA.HeaderText = "FECHA DE RESERVA";
            this.FECHARESERVA.Name = "FECHARESERVA";
            this.FECHARESERVA.ReadOnly = true;
            // 
            // TITULO
            // 
            this.TITULO.HeaderText = "TITULO";
            this.TITULO.Name = "TITULO";
            this.TITULO.ReadOnly = true;
            this.TITULO.Width = 200;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(197, 250);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Eliminar Reserva";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Reservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 295);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.reservasTable);
            this.Name = "Reservas";
            this.Text = "Reservas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Reservas_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.reservasTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView reservasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDRESERVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHARESERVA;
        private System.Windows.Forms.DataGridViewTextBoxColumn TITULO;
        private System.Windows.Forms.Button button1;
    }
}