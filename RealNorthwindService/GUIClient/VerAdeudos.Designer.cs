﻿namespace GUIClient
{
    partial class VerAdeudos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rentasTable = new System.Windows.Forms.DataGridView();
            this.IDRENTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHADERENTA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FECHAENTREG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COSTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONADEUDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.rentasTable)).BeginInit();
            this.SuspendLayout();
            // 
            // rentasTable
            // 
            this.rentasTable.AllowUserToAddRows = false;
            this.rentasTable.AllowUserToDeleteRows = false;
            this.rentasTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rentasTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDRENTA,
            this.FECHADERENTA,
            this.FECHAENTREG,
            this.COSTO,
            this.CONADEUDO});
            this.rentasTable.Location = new System.Drawing.Point(9, 12);
            this.rentasTable.MultiSelect = false;
            this.rentasTable.Name = "rentasTable";
            this.rentasTable.ReadOnly = true;
            this.rentasTable.Size = new System.Drawing.Size(609, 388);
            this.rentasTable.TabIndex = 1;
            // 
            // IDRENTA
            // 
            this.IDRENTA.HeaderText = "IDRENTA";
            this.IDRENTA.Name = "IDRENTA";
            this.IDRENTA.ReadOnly = true;
            // 
            // FECHADERENTA
            // 
            this.FECHADERENTA.HeaderText = "FECHA DE RENTA";
            this.FECHADERENTA.Name = "FECHADERENTA";
            this.FECHADERENTA.ReadOnly = true;
            // 
            // FECHAENTREG
            // 
            this.FECHAENTREG.HeaderText = "FECHA DE ENTREGA";
            this.FECHAENTREG.Name = "FECHAENTREG";
            this.FECHAENTREG.ReadOnly = true;
            // 
            // COSTO
            // 
            this.COSTO.HeaderText = "COSTO";
            this.COSTO.Name = "COSTO";
            this.COSTO.ReadOnly = true;
            // 
            // CONADEUDO
            // 
            this.CONADEUDO.HeaderText = "CON ADEUDO";
            this.CONADEUDO.Name = "CONADEUDO";
            this.CONADEUDO.ReadOnly = true;
            // 
            // VerAdeudos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 412);
            this.Controls.Add(this.rentasTable);
            this.Name = "VerAdeudos";
            this.Text = "VerAdeudos";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VerAdeudos_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.rentasTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView rentasTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDRENTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHADERENTA;
        private System.Windows.Forms.DataGridViewTextBoxColumn FECHAENTREG;
        private System.Windows.Forms.DataGridViewTextBoxColumn COSTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONADEUDO;
    }
}