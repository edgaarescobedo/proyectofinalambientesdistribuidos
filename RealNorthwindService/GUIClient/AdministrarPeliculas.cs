﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class AdministrarPeliculas : Form
    {

        ProductServiceClient client = new ProductServiceClient();
        Form padre;

        public AdministrarPeliculas(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            llenarTabla();
        }

        private void ingresarRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            //Se tiene que deshabilitar la opción de eliminar
            peliculasTable.Enabled = false;
            eliminarButton.Enabled = false;

            //Se obtiene el siguiente id
            ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE = (SELECT MAX(NOSERIE) FROM INVENTARIOPELICULAS2);", new string[0], new string[0]);
            int noserie;
            if (ap.peliculas.Length == 0)
            {
                noserie = 1;
            }
            else
            {
                noserie = ap.peliculas[0].noserie + 1;
            }
            

            //Se llena la información
            limpiarDatos();
            noserieTextbox.Text = noserie + "";


        }

        private void limpiarDatos()
        {
            noserieTextbox.Text = "";
            tituloTextbox.Text = "";
            directoresTextbox.Text = "";
            actoresTextbox.Text = "";
            idiomaoriginalTextbox.Text = "";
            idiomasdisponiblesTextbox.Text = "";
            generoTextbox.Text = "";
            tipoCombobox.Text = "";
        }

        private void actualizarRadiobutton_CheckedChanged(object sender, EventArgs e)
        {
            peliculasTable.Enabled = true;
            peliculaSeleccionada();
        }

        //UTILIZADO PARA LLENAR LA TABLA DE INFORMACIÓN
        private void llenarTabla()
        {
            peliculasTable.ClearSelection();

            //Limpiando la tabla
            peliculasTable.Rows.Clear();

            peliculasTable.AllowUserToAddRows = true;

            //Se obtienen las películas
            ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2;", new string[0], new string[0]);

            //Se llena la tabla con los socios obtenidos
            for (int i = 0; i < ap.peliculas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(peliculasTable);
                row.Cells[0].Value = ap.peliculas[i].noserie;
                row.Cells[1].Value = ap.peliculas[i].titutlo;
                row.Cells[2].Value = ap.peliculas[i].directores;
                row.Cells[3].Value = ap.peliculas[i].actoresprincipales;

                peliculasTable.Rows.Add(row);
            }

            peliculasTable.AllowUserToAddRows = false;
        }

        private void peliculaSeleccionada()
        {
            //Se procede a llenar con información los campos
            DataGridViewRow row = peliculasTable.Rows[peliculasTable.SelectedCells[0].RowIndex];

            //Se obtiene la película seleccionada
            string query = "SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE;";
            string[] p = {"@NOSERIE"};
            string[] v = {row.Cells[0].Value.ToString()};
            ArrayPeliculas ap = client.getPeliculasWithQuery(query, p, v);

            noserieTextbox.Text = ap.peliculas[0].noserie+"";
            tituloTextbox.Text = ap.peliculas[0].titutlo;
            anodefilmacionSpinbox.Value = ap.peliculas[0].anodefilmacion;
            directoresTextbox.Text = ap.peliculas[0].directores;
            actoresTextbox.Text = ap.peliculas[0].actoresprincipales;
            idiomaoriginalTextbox.Text = ap.peliculas[0].idiomaoriginal;
            idiomasdisponiblesTextbox.Text = ap.peliculas[0].idiomasdisponibles;
            generoTextbox.Text = ap.peliculas[0].genero;
            duracionSpinbox.Value = (decimal)ap.peliculas[0].duracion;
            tipoCombobox.Text = ap.peliculas[0].tipo;

            //Solo cuando hay una pelicula seleccionada se puede habilitar el botón de eliminar
            eliminarButton.Enabled = true;
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            eliminarButton.Enabled = false;

            //Se necesita verificar que no haya registros de dicha película para poder eliminarla
            string query = "SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE NOT IN (SELECT NOSERIE FROM RENTAS) AND NOSERIE NOT IN (SELECT NOSERIE FROM COPIAS) AND NOSERIE NOT IN (SELECT NOSERIE FROM RESERVAS) AND NOSERIE=@NOSERIE;";
            string[] parameters = { "@NOSERIE" };
            string[] values = { noserieTextbox.Text };
            ArrayPeliculas ap = client.getPeliculasWithQuery(query, parameters, values);

            //Si no se encuentra dicha película es por que ya hay registros con ese numero de serie y la película no se puede borrar
            if (ap.peliculas.Length == 0)
            {
                MessageBox.Show("La película ya cuenta con registros en la base de datos, estos necesitan ser eliminados antes de poder eliminar la película");
                return;
            }

            
            bool resultado = client.executeNonQuery("DELETE FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE", 
                                                    new string[]{"@NOSERIE"}, 
                                                    new string[]{noserieTextbox.Text});

            if (!resultado)
            {
                MessageBox.Show("No se pudo realizar la petición");
                return;
            }

            //En caso de que se haya podido realizar todo exitosamente se carga de nuevo la tabla
            limpiarDatos();
            llenarTabla();

        }

        private void ingresarinformacionButton_Click(object sender, EventArgs e)
        {
            if(tipoCombobox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un tipo de película válido");
                return;
            }
            if (tituloTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un titulo válido");
                return;
            }
            if (directoresTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese directores");
                return;
            }
            if (idiomaoriginalTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese idiomas originales");
                return;
            }
            if (idiomasdisponiblesTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese idiomas disponibles");
                return;
            }
            if (generoTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese generos");
                return;
            }
            

            eliminarButton.Enabled = false;

            //Ingresar una nueva película
            if (ingresarRadiobutton.Checked)
            {
                string query = "INSERT INTO INVENTARIOPELICULAS2 (NOSERIE, TITULO, ANODEFILMACION, DIRECTORES, ACTORESPRINCIPALES, IDIOMAORIGINAL, IDIOMASDISPONIBLES, GENERO, DURACION, TIPO) VALUES (@NOSERIE, @TITULO, @ANODEFILMACION, @DIRECTORES, @ACTORESPRINCIPALES, @IDIOMAORIGINAL, @IDIOMASDISPONIBLES, @GENERO, @DURACION, @TIPO)";
                string[] parameters = {"@NOSERIE", "@TITULO", "@ANODEFILMACION", "@DIRECTORES", "@ACTORESPRINCIPALES", "@IDIOMAORIGINAL", "@IDIOMASDISPONIBLES", "@GENERO","@DURACION", "@TIPO"};
                string[] values = {noserieTextbox.Text,
                                  tituloTextbox.Text,
                                  anodefilmacionSpinbox.Value+"",
                                  directoresTextbox.Text,
                                  actoresTextbox.Text,
                                  idiomaoriginalTextbox.Text,
                                  idiomasdisponiblesTextbox.Text,
                                  generoTextbox.Text,
                                  duracionSpinbox.Value+"",
                                  tipoCombobox.Text};

                bool resultado = client.executeNonQuery(query, parameters, values);
                if (!resultado)
                {
                    MessageBox.Show("Ocurrió un problema al ingresar la información en el servidor");
                    return;
                }

                limpiarDatos();
                llenarTabla();
            }
            //Actualizar información de una película
            else if (actualizarRadiobutton.Checked)
            {
                string query = "UPDATE INVENTARIOPELICULAS2 SET TITULO=@TITULO, ANODEFILMACION=@ANODEFILMACION, DIRECTORES=@DIRECTORES, ACTORESPRINCIPALES=@ACTORESPRINCIPALES, IDIOMAORIGINAL=@IDIOMAORIGINAL, IDIOMASDISPONIBLES=@IDIOMASDISPONIBLES, GENERO=@GENERO, DURACION=@DURACION, TIPO=@TIPO WHERE NOSERIE=@NOSERIE";
                string[] parameters = { "@NOSERIE", "@TITULO", "@ANODEFILMACION", "@DIRECTORES", "@ACTORESPRINCIPALES", "@IDIOMAORIGINAL", "@IDIOMASDISPONIBLES", "@GENERO", "@DURACION", "@TIPO" };
                string[] values = {noserieTextbox.Text,
                                  tituloTextbox.Text,
                                  anodefilmacionSpinbox.Value+"",
                                  directoresTextbox.Text,
                                  actoresTextbox.Text,
                                  idiomaoriginalTextbox.Text,
                                  idiomasdisponiblesTextbox.Text,
                                  generoTextbox.Text,
                                  duracionSpinbox.Value+"",
                                  tipoCombobox.Text};

                bool resultado = client.executeNonQuery(query, parameters, values);
                if (!resultado)
                {
                    MessageBox.Show("Ocurrió un problema al ingresar la información en el servidor");
                    return;
                }

                limpiarDatos();
                llenarTabla();
            }
        }

        private void AdministrarPeliculas_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void peliculasTable_MouseClick(object sender, MouseEventArgs e)
        {
            peliculaSeleccionada();
        }

        private void peliculasTable_KeyDown(object sender, KeyEventArgs e)
        {
            peliculaSeleccionada();
        }

        private void peliculasTable_KeyUp(object sender, KeyEventArgs e)
        {
            peliculaSeleccionada();
        }
    }


    

}
