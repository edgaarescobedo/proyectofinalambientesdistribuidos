﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;
//crear un objeto de la clase ProductService cuando se crea el form

namespace GUIClient
{
    public partial class EmpleadoLogin : Form
    {
        ProductServiceClient client = new ProductServiceClient();

        public EmpleadoLogin()
        {
            InitializeComponent();
            passTextbox.PasswordChar = '*';

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        //ENTRANDO
        private void button1_Click(object sender, EventArgs e)
        {
            //Si alguno de los dos está vacío entonces no se continúa
            if (usuarioTextbox.Text == "" || passTextbox.Text == "")
            {
                return;
            }

            //Se procede a adquirir al usuario de la base de datos
            try
            {
                Usuario user = client.getUsuario(usuarioTextbox.Text);

                //Se tiene que verificar que la contraseña esté correcta
                if (passTextbox.Text != user.password)
                {
                    MessageBox.Show("Empleado incorrecto", "Usuario o contrasña incorrectos", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    
                    //Se limpia la caja de texto
                    usuarioTextbox.Text = "";
                    passTextbox.Text = "";

                    return;
                }

                new HomePageUsuario().Show();
                this.Dispose();
            }
            catch (Exception)
            {
                //Si se llega hasta aquí es por que el socio no se ha encontrado entonces se tiene que mostrar mensaje al usuario
                MessageBox.Show("Empleado incorrecto", "Usuario o contrasña incorrectos", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                //Se limpia la caja de texto
                usuarioTextbox.Text = "";
                passTextbox.Text = "";
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void EmpleadoLogin_Load(object sender, EventArgs e)
        {
            
        }

        private void EmpleadoLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

    }
}
