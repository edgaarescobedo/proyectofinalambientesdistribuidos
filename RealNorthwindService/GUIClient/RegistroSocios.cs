﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;
using System.Collections;

namespace GUIClient
{
    public partial class RegistroSocios : Form
    {
        ProductServiceClient client = new ProductServiceClient();
        Form padre;

        public RegistroSocios(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;
        }

        private void RegistroSocios_Load(object sender, EventArgs e)
        {
            llenarTabla();

        }

        private void llenarTabla()
        {
            try
            {
                dataGridView1.ClearSelection();

                //Limpiando la tabla
                dataGridView1.Rows.Clear();

                dataGridView1.AllowUserToAddRows = true;

                //Se obtienen los socios
                SociosArray sa = client.getAllSocios();

                //MessageBox.Show("Cantidad de socios: " + sa.socios.Length);

                //Se llena la tabla con los socios obtenidos
                for (int i = 0; i < sa.socios.Length; ++i)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView1);
                    row.Cells[0].Value = sa.socios[i].id;
                    row.Cells[1].Value = sa.socios[i].nombre;
                    row.Cells[2].Value = sa.socios[i].nacimiento;

                    dataGridView1.Rows.Add(row);
                }

                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception e)
            {
                MessageBox.Show("No se encuentran socios en la base de datos \n" + e);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void RegistroSocios_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }


        //Este método es mandado llamar cuando un socio ha sido seleccionado en la tabla
        private void socioSeleccionado()
        {
            //Se obtiene el rango seleccionado
            DataGridViewRow row = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex];

            //Se obtiene el id de dicho elemento y se pide al servidor
            int id = Convert.ToInt32(row.Cells[0].Value);

            //Este método no puede fallar por que es un socio que ya existe
            Socio s = client.getSocio(id);

            //Se llenan los cambios relacionados
            socioTextbox.Text = s.id + "";
            socioTextbox.ReadOnly = true;
            nombreTextbox.Text = s.nombre;
            documentoCombobox.Text = s.identificacion;
            nacimientoDatepicker.Value = s.nacimiento;
            domicilioTextbox.Text = s.domicilio;
            inicioDatepicker.Value = s.registro;
            numeroTextbox.Text = s.numero;
            passwordTextbox.Text = s.password;
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            socioSeleccionado();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            
        }

        private void ingresarButton_Click(object sender, EventArgs e)
        {
            //Se tiene que validar los datos
            if (passwordTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un password valido");
                return;
            }
            if (nombreTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un nombre válido");
                return;
            }

            if (domicilioTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un domicilio válido");
                return;
            }

            if (numeroTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un número válido");
                return;
            }

            //Se crea un socio a partir de la información que se encuntra en el form
            Socio s = new Socio();
            s.id = Convert.ToInt32(socioTextbox.Text);
            s.nombre = nombreTextbox.Text;
            s.numero = numeroTextbox.Text;
            s.identificacion = documentoCombobox.Text;
            s.nacimiento = nacimientoDatepicker.Value;
            s.domicilio = domicilioTextbox.Text;
            s.registro = inicioDatepicker.Value;
            s.password = passwordTextbox.Text;

            //Si se llegó hasta aquí es por que todos los datos son válidos y se puede proseguir
            if (actualizarRadioButton.Checked)
            {
                bool val = client.updateSocio(s);
                //En caso de haber tenido un problema al momento de hacer el update se tiene que avisar al usuario
                if (!val)
                {
                    MessageBox.Show("Hubo un problema al hacer la actualización en el servidor");
                    return;
                }

                //En caso de que se haya realizado de forma correcta la actualización se hace un update de la tabla
                llenarTabla();
            }
            else
            {
                bool val = client.insertSocio(s);

                if (!val)
                {
                    MessageBox.Show("Hubo un problema al insertar el socio en el servidor");
                    return;
                }

                //Se actualiza la tabla
                llenarTabla();
            }



        }

        private void agregarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            //Se dehsabilita el datagrid
            dataGridView1.Enabled = false;

            //Se tienen que limpiar los campos
            nombreTextbox.Text = "";
            domicilioTextbox.Text = "";
            numeroTextbox.Text = "";

            //Se ingresa el número de usuario correspondiente a una nueva incersión
            socioTextbox.Text = client.getNextSocioID() + "";

        }

        private void actualizarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            dataGridView1.Enabled = true;
            socioSeleccionado();
        }

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            socioSeleccionado();
        }

        private void dataGridView1_KeyUp(object sender, KeyEventArgs e)
        {
            socioSeleccionado();
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 0)
            {
                return;
            }

                

            string[] p = { "@IDSOCIO" };
            object[] v = { dataGridView1.SelectedCells[0].OwningRow.Cells[0].Value.ToString() };

            SociosArray sa = client.getSociosWithQuery("SELECT * FROM SOCIOS WHERE ID NOT IN (SELECT IDSOCIO FROM RENTAS) AND ID NOT IN (SELECT SOCIO FROM RESERVAS) AND ID=@IDSOCIO;", p, v);

            if (sa.socios.Length == 0)
            {
                MessageBox.Show("El socio ya tiene registros en el sistema, necesitan ser eliminados antes de eliminar al socio");
                return;
            }

            bool resultado = client.executeNonQuery("DELETE FROM SOCIOS WHERE ID=@IDSOCIO;", p, v);

            if (!resultado)
            {
                MessageBox.Show("Problema al realizar la operación con el servidor.");
                return;
            }

            llenarTabla();
            

        }




    }
}
