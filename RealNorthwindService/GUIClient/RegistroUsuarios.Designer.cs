﻿namespace GUIClient
{
    partial class RegistroUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usuariosTable = new System.Windows.Forms.DataGridView();
            this.IDUSUARIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TIPO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PASSWORD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.idTexbox = new System.Windows.Forms.TextBox();
            this.passwordTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ingresarRadiobutton = new System.Windows.Forms.RadioButton();
            this.actualizarRadiobutton = new System.Windows.Forms.RadioButton();
            this.eliminarButton = new System.Windows.Forms.Button();
            this.enterButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tipoCombobox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.usuariosTable)).BeginInit();
            this.SuspendLayout();
            // 
            // usuariosTable
            // 
            this.usuariosTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usuariosTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDUSUARIO,
            this.TIPO,
            this.PASSWORD});
            this.usuariosTable.Enabled = false;
            this.usuariosTable.Location = new System.Drawing.Point(12, 12);
            this.usuariosTable.MultiSelect = false;
            this.usuariosTable.Name = "usuariosTable";
            this.usuariosTable.ReadOnly = true;
            this.usuariosTable.Size = new System.Drawing.Size(345, 109);
            this.usuariosTable.TabIndex = 0;
            this.usuariosTable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.usuariosTable_KeyDown);
            this.usuariosTable.KeyUp += new System.Windows.Forms.KeyEventHandler(this.usuariosTable_KeyUp);
            this.usuariosTable.MouseClick += new System.Windows.Forms.MouseEventHandler(this.usuariosTable_MouseClick);
            // 
            // IDUSUARIO
            // 
            this.IDUSUARIO.HeaderText = "IDUSUARIO";
            this.IDUSUARIO.Name = "IDUSUARIO";
            this.IDUSUARIO.ReadOnly = true;
            // 
            // TIPO
            // 
            this.TIPO.HeaderText = "TIPO";
            this.TIPO.Name = "TIPO";
            this.TIPO.ReadOnly = true;
            // 
            // PASSWORD
            // 
            this.PASSWORD.HeaderText = "PASSWORD";
            this.PASSWORD.Name = "PASSWORD";
            this.PASSWORD.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ID";
            // 
            // idTexbox
            // 
            this.idTexbox.Location = new System.Drawing.Point(156, 186);
            this.idTexbox.Name = "idTexbox";
            this.idTexbox.Size = new System.Drawing.Size(121, 20);
            this.idTexbox.TabIndex = 2;
            // 
            // passwordTextbox
            // 
            this.passwordTextbox.Location = new System.Drawing.Point(156, 255);
            this.passwordTextbox.Name = "passwordTextbox";
            this.passwordTextbox.Size = new System.Drawing.Size(121, 20);
            this.passwordTextbox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(97, 258);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // ingresarRadiobutton
            // 
            this.ingresarRadiobutton.AutoSize = true;
            this.ingresarRadiobutton.Checked = true;
            this.ingresarRadiobutton.Location = new System.Drawing.Point(12, 127);
            this.ingresarRadiobutton.Name = "ingresarRadiobutton";
            this.ingresarRadiobutton.Size = new System.Drawing.Size(156, 17);
            this.ingresarRadiobutton.TabIndex = 5;
            this.ingresarRadiobutton.TabStop = true;
            this.ingresarRadiobutton.Text = "Ingresar Nueva Información";
            this.ingresarRadiobutton.UseVisualStyleBackColor = true;
            this.ingresarRadiobutton.CheckedChanged += new System.EventHandler(this.ingresarRadiobutton_CheckedChanged);
            // 
            // actualizarRadiobutton
            // 
            this.actualizarRadiobutton.AutoSize = true;
            this.actualizarRadiobutton.Location = new System.Drawing.Point(12, 150);
            this.actualizarRadiobutton.Name = "actualizarRadiobutton";
            this.actualizarRadiobutton.Size = new System.Drawing.Size(129, 17);
            this.actualizarRadiobutton.TabIndex = 6;
            this.actualizarRadiobutton.Text = "Actualizar Información";
            this.actualizarRadiobutton.UseVisualStyleBackColor = true;
            this.actualizarRadiobutton.CheckedChanged += new System.EventHandler(this.actualizarRadiobutton_CheckedChanged);
            // 
            // eliminarButton
            // 
            this.eliminarButton.Location = new System.Drawing.Point(282, 127);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Size = new System.Drawing.Size(75, 23);
            this.eliminarButton.TabIndex = 7;
            this.eliminarButton.Text = "Eliminar";
            this.eliminarButton.UseVisualStyleBackColor = true;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // enterButton
            // 
            this.enterButton.Location = new System.Drawing.Point(135, 291);
            this.enterButton.Name = "enterButton";
            this.enterButton.Size = new System.Drawing.Size(75, 23);
            this.enterButton.TabIndex = 8;
            this.enterButton.Text = "Ingresar Información";
            this.enterButton.UseVisualStyleBackColor = true;
            this.enterButton.Click += new System.EventHandler(this.enterButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(122, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Tipo";
            // 
            // tipoCombobox
            // 
            this.tipoCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tipoCombobox.FormattingEnabled = true;
            this.tipoCombobox.Items.AddRange(new object[] {
            "ADMIN",
            "VENDEDOR"});
            this.tipoCombobox.Location = new System.Drawing.Point(156, 222);
            this.tipoCombobox.Name = "tipoCombobox";
            this.tipoCombobox.Size = new System.Drawing.Size(121, 21);
            this.tipoCombobox.TabIndex = 10;
            // 
            // RegistroUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 337);
            this.Controls.Add(this.tipoCombobox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.enterButton);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.actualizarRadiobutton);
            this.Controls.Add(this.ingresarRadiobutton);
            this.Controls.Add(this.passwordTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.idTexbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.usuariosTable);
            this.Name = "RegistroUsuarios";
            this.Text = "RegistroUsuarios";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegistroUsuarios_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.usuariosTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView usuariosTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox idTexbox;
        private System.Windows.Forms.TextBox passwordTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton ingresarRadiobutton;
        private System.Windows.Forms.RadioButton actualizarRadiobutton;
        private System.Windows.Forms.Button eliminarButton;
        private System.Windows.Forms.Button enterButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDUSUARIO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TIPO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PASSWORD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox tipoCombobox;
    }
}