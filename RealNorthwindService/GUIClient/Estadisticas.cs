﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class Estadisticas : Form
    {
        ProductServiceClient client = new ProductServiceClient();
        Form padre;

        public Estadisticas(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            double sumarentas = 0.0;
            //Se procede a obtener las estadísticas
            SociosArray sa = client.getAllSocios();
            ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2;", new string[0], new object[0]);
            Tuple<int, int>[] conteos = new Tuple<int, int>[sa.socios.Length];
            Tuple<int, int>[] peliculasmasrentadas = new Tuple<int, int>[ap.peliculas.Length];
            int i = 0;
            foreach(var auto in sa.socios)
            {
                conteos[i] = new Tuple<int, int>(auto.id, 0);
                i++;
            }
            i = 0;
            foreach (var auto in ap.peliculas)
            {
                peliculasmasrentadas[i] =  new Tuple<int, int>(auto.noserie, 0);
                i++;
            }
            //Se procede a hacer el conteo 
            ArrayRentas ar = client.getRentasWithQuery("SELECT * FROM RENTAS", new string[0], new object[0]);
            for (int x = 0; x < ar.rentas.Length; ++x)
            {
                sumarentas += ar.rentas[x].costo;

                for (int s = 0; s < sa.socios.Length; ++s)
                {
                    if (conteos[s].Item1 == ar.rentas[x].idsocio)
                    {
                        conteos[s] = new Tuple<int, int>(conteos[s].Item1, conteos[s].Item2 + 1);
                    }
                }

                for (int s = 0; s < ap.peliculas.Length; ++s)
                {
                    if (peliculasmasrentadas[s].Item1 == ar.rentas[x].noserie)
                    {
                        peliculasmasrentadas[s] = new Tuple<int, int>(peliculasmasrentadas[s].Item1, peliculasmasrentadas[s].Item2 + 1);
                    }
                }
            }
            //Una vez hecho el conteo se saca quien ha sido el socio con más rentas
            int socioconmasrentas = 0;
            int conteorentas = 0;
            foreach (var auto in conteos)
            {
                if (auto.Item2 > conteorentas)
                {
                    socioconmasrentas = auto.Item1;
                    conteorentas = auto.Item2;
                }
            }
            socioconmasrentasTextbox.Text = socioconmasrentas + "";
            totalderentasTextbox.Text = sumarentas + "";

            int peliculaMasRentada = 0;
            int conteopeliculas = 0;
            foreach (var auto in peliculasmasrentadas)
            {
                if (auto.Item2 > conteopeliculas)
                {
                    peliculaMasRentada = auto.Item1;
                    conteopeliculas = auto.Item2;
                }
            }

            peliculamasrentadaTextbox.Text = peliculaMasRentada + "";


            //PARA LAS MULTAS
            double sumaMultas = 0.0;
            sa = client.getAllSocios();
            ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2;", new string[0], new object[0]);
            Tuple<int, int>[] sociosConMultas = new Tuple<int, int>[sa.socios.Length];
            Tuple<int, int>[] peliculasConMultas = new Tuple<int, int>[ap.peliculas.Length];
            i = 0;
            foreach (var auto in sa.socios)
            {
                sociosConMultas[i] = new Tuple<int, int>(auto.id, 0);
                i++;
            }
            i = 0;
            foreach (var auto in ap.peliculas)
            {
                peliculasConMultas[i] = new Tuple<int, int>(auto.noserie, 0);
                i++;
            }
            //Se procede a hacer el conteo 
            ArrayMultas am = client.getMultasWithQuery("SELECT * FROM MULTAS", new string[0], new object[0]);
            for (int x = 0; x < am.multas.Length; ++x)
            {

                //Se tiene que obtener el socio responsable de la multa
                ArrayRentas rr = client.getRentasWithQuery("SELECT * FROM RENTAS WHERE IDRENTA=@IDRENTA;",new string[]{"@IDRENTA"}, new object[]{am.multas[x].idrenta});
                sumaMultas += am.multas[x].multatotal;

                for (int s = 0; s < sa.socios.Length; ++s)
                {
                    if (sociosConMultas[s].Item1 == rr.rentas[0].idsocio)
                    {
                        sociosConMultas[s] = new Tuple<int, int>(sociosConMultas[s].Item1, sociosConMultas[s].Item2 + 1);
                    }
                }

                for (int s = 0; s < ap.peliculas.Length; ++s)
                {
                    if (peliculasConMultas[s].Item1 == rr.rentas[0].noserie)
                    {
                        peliculasConMultas[s] = new Tuple<int, int>(peliculasConMultas[s].Item1, peliculasConMultas[s].Item2 + 1);
                    }
                }
            }
            //Una vez hecho el conteo se saca quien ha sido el socio con más rentas
            int socioConMasMultas = 0;
            int conteoMultas = 0;
            foreach (var auto in sociosConMultas)
            {
                if (auto.Item2 > conteoMultas)
                {
                    socioConMasMultas = auto.Item1;
                    conteoMultas = auto.Item2;
                }
            }
            usuarioconmasmultasTextbox.Text = socioConMasMultas + "";
            totaldemultasTextbox.Text = sumaMultas + "";

            int peliculaConMasMultas = 0;
            int conteoPeliculaConMasMultas = 0;
            foreach (var auto in peliculasConMultas)
            {
                if (auto.Item2 > conteoPeliculaConMasMultas)
                {
                    peliculaConMasMultas = auto.Item1;
                    conteoPeliculaConMasMultas = auto.Item2;
                }
            }

            peliculaconmasmultasTextbox.Text = peliculaConMasMultas + "";

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Estadisticas_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }
    }
}
