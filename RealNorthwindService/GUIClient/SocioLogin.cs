﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class SocioLogin : Form
    {

        ProductServiceClient client = new ProductServiceClient();

        public SocioLogin()
        {
            InitializeComponent();
        }

        private void SocioLogin_Load(object sender, EventArgs e)
        {

        }

        private void SocioLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }


        //ENTRANDO
        private void entrarButton_Click(object sender, EventArgs e)
        {
            //Se tiene que verificar que la casilla no esté sola si no no entra
            if (numeroSocioTextbox.Text == "")
            {
                return;
            }

            int id = Convert.ToInt32(numeroSocioTextbox.Text);

            //Si nó está vacía entonces se procede a verificar si hay algún socio existente
            try
            {
                
                Socio s = client.getSocio(id);
                
                //Se verifica ahora que la contraseña coincida
                if (s.password == passwordTextbox.Text)
                {
                    //Se guarda la información en la clase estática
                    StaticClass.socio = s.id;

                    //Entonces se pasa a la siguiente ventana
                    new HomePageSocio().Show();
                    this.Dispose();
                }
                //En caso de que no coincida la contraseña se tiene que avisar al usuario y limpiar los campos
                else
                {
                    MessageBox.Show("Contraseña incorrecta", "Ingrese la contraseña correcta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    numeroSocioTextbox.Text = "";
                    passwordTextbox.Text = "";
                }
                //En caso de que se haya encontrado exitosamente entonces se procede a pasar a la siguiente ventana...
            }
            catch (Exception)
            {
                //Si se llega hasta aquí es por que el socio no se ha encontrado entonces se tiene que mostrar mensaje al usuario
                MessageBox.Show("Número de socio incorrecto", "No se encuentra ningún socio con ese número", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                //Se limpia la caja de texto
                numeroSocioTextbox.Text = "";
                passwordTextbox.Text = "";
            }

        }

        private void loginLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
