﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class RegresarRenta : Form
    {
        Form padre;
        ProductServiceClient client = new ProductServiceClient();

        public RegresarRenta(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            llenarTabla();
        }

        private void RegresarRenta_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void llenarTabla()
        {
            rentasTable.AllowUserToAddRows = true;

            rentasTable.Rows.Clear();

            //Se obtienen todas las rentas
            ArrayRentas ar = client.getRentasWithQuery("SELECT * FROM RENTAS WHERE ESTADO=@ESTADO;", new string[]{"@ESTADO"},new object[]{"PENDIENTE"});

            for (int i = 0; i < ar.rentas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(rentasTable);

                row.Cells[0].Value = ar.rentas[i].idrenta;
                row.Cells[1].Value = ar.rentas[i].idsocio;
                row.Cells[2].Value = ar.rentas[i].fechaentrega;
                row.Cells[3].Value = ar.rentas[i].costo;
                
                rentasTable.Rows.Add(row);


            }
            
            rentasTable.AllowUserToAddRows = false;
        }

        private void tablaSeleccionada()
        {
            if(rentasTable.SelectedRows.Count == 0)
            {
                return;
            }

            //Se obtiene dicha renta
            DataGridViewRow row = rentasTable.Rows[rentasTable.SelectedCells[0].RowIndex];
            ArrayRentas ar = client.getRentasWithQuery("SELECT * FROM RENTAS WHERE IDRENTA=@IDRENTA", new string[] { "@IDRENTA" }, new object[] { row.Cells[0] });
            
            //Se procede a calcular el monto de la multa en caso de ser así
            DateTime now = DateTime.Now;
            DateTime fechaentrega = ar.rentas[0].fechaentrega;
            int diferenciadedias = Convert.ToInt32((now - fechaentrega).TotalDays);

            //Se calcula el monto a partir de la diferencia de días
            if (diferenciadedias <= 0)
            {
                adeudoTextbox.Text = "0.0";
                return;
            }

            adeudoTextbox.Text = (diferenciadedias * ar.rentas[0].costo) + "";




        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (rentasTable.Rows.Count == 0)
            {
                return;
            }
            if (rentasTable.SelectedCells.Count == 0)
            {
                return;
            }


            DataGridViewRow row = rentasTable.Rows[rentasTable.SelectedCells[0].RowIndex];


            //Si hay algún adeudo entonce se procede a hacer la multa
            if (adeudoTextbox.Text != "0.0")
            {
                //Se consigue el nuevo id de multa
                int idmulta = -1;
                ArrayMultas am = client.getMultasWithQuery("SELECT * FROM MULTAS WHERE IDMULTA = (SELECT MAX(IDMULTA) FROM MULTAS);", new string[0], new object[0]);
                if (am.multas.Length == 0)
                {
                    idmulta = 1;
                }
                else
                {
                    idmulta = am.multas[0].idmulta + 1;
                }

                //SE PROCEDE A HACER LA MULTA
                string[] parameters = new string[] {"@IDMULTA", "@IDSOCIO", "@MULTATOTAL" };
                object[] values = new object[] {idmulta,  row.Cells[1].Value, adeudoTextbox.Text};
                bool resultado = client.executeNonQuery("INSERT INTO MULTAS (IDMULTA, IDSOCIO, MULTATOTAL) VALUES (@IDMULTA, @IDSOCIO, @MULTATOTAL);", parameters, values);

                if (!resultado)
                {
                    MessageBox.Show("Problema al ingresar la multa en el servidor");
                    return;
                }
            }

            //Se procede a cambiar el estado de la renta a entregado
            string[] p2 = new string[] {"@IDRENTA", "@ESTADO" };
            object[] v2 = new object[] {row.Cells[0].Value, "ENTREGADO" };
            bool r2 = client.executeNonQuery("UPDATE RENTAS SET ESTADO=@ESTADO WHERE IDRENTA=@IDRENTA", p2, v2);

            if (!r2)
            {
                MessageBox.Show("Problema el cambiar el estado de la renta en el servidor");
                return;
            }

            //Se carga de nuevo la tabla y se limpia el textbox de adeudo
            llenarTabla();
            adeudoTextbox.Text = "0.0";

        }

        private void rentasTable_MouseClick(object sender, MouseEventArgs e)
        {
            tablaSeleccionada();
        }

        private void rentasTable_KeyDown(object sender, KeyEventArgs e)
        {
            tablaSeleccionada();
        }

        private void rentasTable_KeyUp(object sender, KeyEventArgs e)
        {
            tablaSeleccionada();
        }
    }
}
