﻿namespace GUIClient
{
    partial class Inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.empleadoButton = new System.Windows.Forms.Button();
            this.socioButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // empleadoButton
            // 
            this.empleadoButton.Location = new System.Drawing.Point(94, 31);
            this.empleadoButton.Name = "empleadoButton";
            this.empleadoButton.Size = new System.Drawing.Size(158, 91);
            this.empleadoButton.TabIndex = 0;
            this.empleadoButton.Text = "Empleado";
            this.empleadoButton.UseVisualStyleBackColor = true;
            this.empleadoButton.Click += new System.EventHandler(this.empleadoButton_Click);
            // 
            // socioButton
            // 
            this.socioButton.Location = new System.Drawing.Point(94, 156);
            this.socioButton.Name = "socioButton";
            this.socioButton.Size = new System.Drawing.Size(158, 91);
            this.socioButton.TabIndex = 1;
            this.socioButton.Text = "Socio";
            this.socioButton.UseVisualStyleBackColor = true;
            this.socioButton.Click += new System.EventHandler(this.socioButton_Click);
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 296);
            this.Controls.Add(this.socioButton);
            this.Controls.Add(this.empleadoButton);
            this.Name = "Inicio";
            this.Text = "Inicio";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Inicio_FormClosing);
            this.Load += new System.EventHandler(this.Inicio_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button empleadoButton;
        private System.Windows.Forms.Button socioButton;
    }
}