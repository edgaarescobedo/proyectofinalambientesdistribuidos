﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIClient
{
    public partial class Ticket : Form
    {
        Form padre;
        public Ticket(Form padre, string ticket)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            richTextBox1.Text = ticket;
        }

        private void Ticket_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            padre.Enabled = true;
            this.Dispose();
        }
    }
}
