﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;
using System.Collections;

namespace GUIClient
{
    public partial class RentaPeliculas : Form
    {
        ProductServiceClient client = new ProductServiceClient();
        Form padre;

        public RentaPeliculas(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;
        }

        private void buscarSocioButton_Click(object sender, EventArgs e)
        {
            
            //Se limpia el campo del nombre del socio
            nombresocioTextbox.Text = "";
            
            //Se busca al socio al cual se le hará la cobranza
            SociosArray sa = client.getSociosWithQuery("SELECT * FROM SOCIOS WHERE ID=@ID;", new string[]{"@ID"}, new object[]{socioTextbox.Text});

            if (sa.socios.Length == 0)
            {
                return;
            }

            nombresocioTextbox.Text = sa.socios[0].nombre;


            //Se bloqueará al socio si tiene rentas con más de 10 días de retraso
            string[] p = { "@IDSOCIO", "@ESTADO" };
            object[] v = { socioTextbox.Text, "PENDIENTE" };
            ArrayRentas ar = client.getRentasWithQuery("SELECT * FROM RENTAS WHERE IDSOCIO=@IDSOCIO AND ESTADO=@ESTADO;", p, v);

            DateTime now = DateTime.Now;

            for (int i = 0; i < ar.rentas.Length; ++i)
            {
                if ((ar.rentas[i].fechaentrega - now).TotalDays >= 10)
                {
                    MessageBox.Show("El usuario ha sido bloqueado por adeudos");
                    nombresocioTextbox.Text = "";
                }
            }

            //SE LLENA LA TABLA CON LAS PELICULAS QUE LE GUSTARÍA VER AL USUARIO
            p = new string[]{"@IDSOCIO"};
            v = new object[]{socioTextbox.Text};

            ArrayPeliculas todaslasnovistas = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE NOT IN (SELECT NOSERIE FROM RENTAS WHERE IDSOCIO=@IDSOCIO);", p, v);
            ArrayPeliculas peliculasvistas = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE IN (SELECT NOSERIE FROM RENTAS WHERE IDSOCIO=@IDSOCIO);", p, v);
            if (peliculasvistas.peliculas.Length == 0)
            {
                return;
            }
            Random r = new Random(DateTime.Now.Millisecond);
            int index = r.Next(0, peliculasvistas.peliculas.Length);
            Pelicula peliculaelegida = peliculasvistas.peliculas[index];

            ArrayList concuerdan = new ArrayList();
            for (int i = 0; i < todaslasnovistas.peliculas.Length; ++i)
            {
                if (todaslasnovistas.peliculas[i].genero.Contains(peliculaelegida.genero))
                {
                    concuerdan.Add(todaslasnovistas.peliculas[i]);
                }
            }

            //Se procede a llenar la tabla
            tepuedeinteresarTable.AllowUserToAddRows = true;
            tepuedeinteresarTable.Rows.Clear();
            for (int i = 0; i < concuerdan.Count; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(tepuedeinteresarTable);

                Pelicula pp = (Pelicula)concuerdan[i];

                row.Cells[0].Value = pp.titutlo;
                row.Cells[1].Value = pp.anodefilmacion;
                row.Cells[2].Value = pp.actoresprincipales;
                row.Cells[3].Value = pp.directores;

                tepuedeinteresarTable.Rows.Add(row);
            }

            tepuedeinteresarTable.AllowUserToAddRows = false;

        }

        private void RentaPeliculas_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void calcularCosto()
        {

            double suma = 0;

            for (int i = 0; i < peliculasarentarTable.Rows.Count; ++i)
            {
                DataGridViewRow row = peliculasarentarTable.Rows[i];
                suma += Convert.ToDouble(row.Cells[2].Value);
            }

            totalTextbox.Text = suma + "";
        }

        private void ingresarpeliculaButton_Click(object sender, EventArgs e)
        {
            //Se busca la película dependiendo de su id
            ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE;", new string[] { "@NOSERIE" }, new object[] { noserieTexbox.Text });

            //Se procede a obtener el número de copias disponibles para esa pelicula
            string cm;
            cm = "SELECT * FROM COPIAS WHERE IDCOPIA NOT IN (SELECT IDCOPIA FROM BAJAS) AND IDCOPIA NOT IN (SELECT IDCOPIA FROM RENTAS WHERE ESTADO = 'PENDIENTE') AND NOSERIE=@NOSERIE";
            string[] parameters = new string[1];
            string[] values = new string[parameters.Length];
            parameters[0] = "@NOSERIE";
            values[0] = noserieTexbox.Text;
            ArrayCopias copias = client.getCopiasWithQuery(cm, parameters, values);

            //Si no hay copias disponibles no se puede realizar la renta
            if (copias.copias.Length == 0)
            {
                MessageBox.Show("No hay copias disponibles para esa película");
                return;
            }

            //Se busca por una copia que no se encuentre en ya en la tabla
            ArrayList copiasid = new ArrayList();
            for (int i = 0; i < peliculasarentarTable.Rows.Count; ++i)
            {
                copiasid.Add(Convert.ToInt32(peliculasarentarTable.Rows[i].Cells[3].Value));
            }

            int idcopiaagregar = -1;
            for (int i = 0; i < copias.copias.Length; ++i)
            {
                bool contiene = false;

                for (int x = 0; x < copiasid.Count; ++x)
                {
                    if (Convert.ToInt32(copiasid[x]) == copias.copias[i].idcopia)
                    {
                        contiene = true;
                        break;
                    }
                }

                if(!contiene)
                {
                    idcopiaagregar = copias.copias[i].idcopia;
                    break;
                }
            }


            if (idcopiaagregar == -1)
            {
                MessageBox.Show("No hay copias disponibles para esta película");
                return;
            }



            if (ap.peliculas.Length == 0)
            {
                return;
            }

            peliculasarentarTable.AllowUserToAddRows = true;

            //Se crea una nueva entrada en la lista de peliculas por rentar
            DataGridViewRow row = new DataGridViewRow();
            row.CreateCells(peliculasarentarTable);

            row.Cells[0].Value = ap.peliculas[0].noserie;
            row.Cells[1].Value = ap.peliculas[0].titutlo;

            //Se hace la busqueda para saber el costo
            ArrayCostos ac = client.getCostosWithQuery("SELECT * FROM COSTOS WHERE TIPO=@TIPO", new string[] { "@TIPO" }, new string[] { ap.peliculas[0].tipo });
            row.Cells[2].Value = ac.costos[0].precio;

            //Se agrega el id de la copia disponible
            row.Cells[3].Value = idcopiaagregar;

            //Se agrega el registro a la tabla
            peliculasarentarTable.Rows.Add(row);

            //Se calcula cuanto se le va a cobrar al socio
            calcularCosto();

            peliculasarentarTable.AllowUserToAddRows = false;
        }

        private void quitarpeliculaButton_Click(object sender, EventArgs e)
        {
            if(peliculasarentarTable.Rows.Count == 0)
            {
                return;
            }

            if (peliculasarentarTable.SelectedCells.Count == 0)
            {
                return;
            }

            peliculasarentarTable.Rows.RemoveAt(peliculasarentarTable.SelectedCells[0].RowIndex);

            calcularCosto();
        }

        private void buscarButton_Click(object sender, EventArgs e)
        {


            string query = "SELECT * FROM INVENTARIOPELICULAS2";
            //Se manda a ejecutar el comando
            ArrayPeliculas ap = client.getPeliculasWithQuery(query, new string[0], new string[0]);

            //Arraylist para guardar la selección
            ArrayList seleccion = new ArrayList();

            //Se hace la búsqueda dependiendo de lo que haya seleccionado el usuario
            for (int i = 0; i < ap.peliculas.Length; ++i)
            {
                Pelicula p = ap.peliculas[i];
                bool agregar = false;

                if (tituloRadioButton.Checked)
                {
                    agregar = p.titutlo.Contains(textBox.Text);
                }
                else if (generoRadioButton.Checked)
                {
                    agregar = p.genero.Contains(textBox.Text);
                }
                else if (anoRadioButton.Checked)
                {
                    try
                    {
                        int ano = Convert.ToInt32(textBox.Text);
                        agregar = p.anodefilmacion == ano;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Por favor ingrese un año válido (sólo números)");
                        return;
                    }
                }
                else if (actorRadioButton.Checked)
                {
                    agregar = p.actoresprincipales.Contains(textBox.Text);
                }
                else if (directorRadioButton.Checked)
                {
                    agregar = p.directores.Contains(textBox.Text);
                }

                //Si es que cumple con las condiciones entonces se agrega al arraylist
                if (agregar)
                {
                    seleccion.Add(p);
                    //MessageBox.Show("Se ha agregado una película");
                }


            }//FOR END

            //Convirtiendo de arraylist a array
            Pelicula[] peliculas = new Pelicula[seleccion.Count];
            for (int i = 0; i < peliculas.Length; ++i)
            {
                peliculas[i] = (Pelicula)seleccion[i];
            }

            //Se procede a llenar la tabla
            todaslaspeliculasTable.ClearSelection();

            //Limpiando la tabla
            todaslaspeliculasTable.Rows.Clear();

            //Se habilita la posibilidad de añadir registros
            todaslaspeliculasTable.AllowUserToAddRows = true;

            //Se llena la tabla con las películas ingresadas
            for (int i = 0; i < peliculas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(todaslaspeliculasTable);
                row.Cells[0].Value = peliculas[i].noserie;
                row.Cells[1].Value = peliculas[i].titutlo;


                todaslaspeliculasTable.Rows.Add(row);
            }

            //Se vuelve a deshabilitar la opción de añadir filas
            todaslaspeliculasTable.AllowUserToAddRows = false;
        }

        private void cobrarButton_Click(object sender, EventArgs e)
        {
            //Se tiene que checar que el apartado del nombre no esté vacío
            if (nombresocioTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un socio válido");
                return;
            }

            //Se checa si hay películas por rentar
            if (peliculasarentarTable.Rows.Count == 0)
            {
                MessageBox.Show("Por favor ingresa películas a rentar");
                return;
            }

            //Se procede a hacer las renta del usuario
            string ticket = "Renta al socio " + socioTextbox.Text + "\n";
            ticket += "Al día: " + DateTime.Now + "\n\n";
            for (int i = 0; i < peliculasarentarTable.Rows.Count; ++i)
            {
                
                //Se obtienen la pelicula, el costo, el id de la renta
                ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE;", new string[] { "@NOSERIE" }, new object[] { peliculasarentarTable.Rows[i].Cells[0].Value });
                Pelicula pelicula = ap.peliculas[0];
 
                ArrayCostos ac = client.getCostosWithQuery("SELECT * FROM COSTOS WHERE TIPO=@TIPO;", new string[]{"@TIPO"}, new string[]{pelicula.tipo});
                Costo costo = ac.costos[0];

                //Se obtiene el siguiente id de la renta
                ArrayRentas rs = client.getRentasWithQuery("SELECT * FROM RENTAS WHERE IDRENTA = (SELECT MAX(IDRENTA) FROM RENTAS);", new string[0], new object[0]);
                int idrenta = -1;
                if (rs.rentas.Length == 0)
                {
                    idrenta = 1;
                }
                else
                {
                    idrenta = rs.rentas[0].idrenta + 1;
                }


                //Se obtiene la fecha
                DateTime hoy = DateTime.Now;

                ticket += "IDRENTA:" + idrenta+"\n";
                ticket += "IDCOPIA: " + peliculasarentarTable.Rows[i].Cells[3].Value + "\n";
                ticket += "Titulo: " + peliculasarentarTable.Rows[i].Cells[1].Value + "\n";
                ticket += "Fecha de Entrega" + (hoy.AddHours(costo.tiemporenta)) + "\n";
                ticket += "Costo: $" + costo.precio + "\n";

                ticket += "=====================================================\n\n";

                
                string[] parameters = { "@IDCOPIA", "@IDRENTA", "@IDSOCIO", "@NOSERIE", "@COSTO", "@ESTADO", "@FECHA", "@FECHAENTREGA" };
                object[] values = {peliculasarentarTable.Rows[i].Cells[3].Value, idrenta, socioTextbox.Text, pelicula.noserie, costo.precio, "PENDIENTE", hoy, (hoy.AddHours(costo.tiemporenta))};
                bool resultado = client.executeNonQuery("INSERT INTO RENTAS (IDCOPIA, IDRENTA, IDSOCIO, NOSERIE, COSTO, ESTADO, FECHA, FECHAENTREGA) VALUES (@IDCOPIA, @IDRENTA, @IDSOCIO, @NOSERIE, @COSTO, @ESTADO, @FECHA, @FECHAENTREGA);", parameters, values);

                if (!resultado)
                {
                    MessageBox.Show("Problema en el servidor no se pudo guarda la renta con IDCOPIA:" + peliculasarentarTable.Rows[i].Cells[3]);
                    return;
                }
            }//END FOR

            //Se limpia la tabla de rentas
            peliculasarentarTable.AllowUserToAddRows = true;
            peliculasarentarTable.Rows.Clear();
            peliculasarentarTable.AllowUserToAddRows = false;

            ticket += "COSTO TOTAL: $" + totalTextbox.Text;

            //Se muestra el ticket de la cobranza
            new Ticket(this, ticket).Show();

            totalTextbox.Text = "";
        }

        private void socioTextbox_TextChanged(object sender, EventArgs e)
        {
            nombresocioTextbox.Text = "";

            tepuedeinteresarTable.AllowUserToAddRows = true;
            tepuedeinteresarTable.Rows.Clear();
            tepuedeinteresarTable.AllowUserToAddRows = false;
        }
    }
}
