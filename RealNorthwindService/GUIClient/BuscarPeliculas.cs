﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.ServiceModel;
using GUIClient.ServiceRef;
using System.Collections;

namespace GUIClient
{
    public partial class BuscarPeliculas : Form
    {

        ProductServiceClient client = new ProductServiceClient();

        Form padre;

        public BuscarPeliculas(Form padre)
        {
            InitializeComponent();
            this.padre = padre;

            padre.Enabled = false;

            reservaDatetime.MinDate = DateTime.Now;

            llenarTepuedeinteresar();
        }

        private void llenarTepuedeinteresar()
        {
            //SE LLENA LA TABLA CON LAS PELICULAS QUE LE GUSTARÍA VER AL USUARIO
            string[] p = new string[] { "@IDSOCIO" };
            object[] v = new object[] { StaticClass.socio};

            ArrayPeliculas todaslasnovistas = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE NOT IN (SELECT NOSERIE FROM RENTAS WHERE IDSOCIO=@IDSOCIO);", p, v);
            ArrayPeliculas peliculasvistas = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE IN (SELECT NOSERIE FROM RENTAS WHERE IDSOCIO=@IDSOCIO);", p, v);
            Random r = new Random(DateTime.Now.Millisecond);
            int index = r.Next(0, peliculasvistas.peliculas.Length);
            Pelicula peliculaelegida = peliculasvistas.peliculas[index];

            ArrayList concuerdan = new ArrayList();
            for (int i = 0; i < todaslasnovistas.peliculas.Length; ++i)
            {
                if (todaslasnovistas.peliculas[i].genero.Contains(peliculaelegida.genero))
                {
                    concuerdan.Add(todaslasnovistas.peliculas[i]);
                }
            }

            //Se procede a llenar la tabla
            tepuedeinteresarTable.AllowUserToAddRows = true;
            tepuedeinteresarTable.Rows.Clear();
            for (int i = 0; i < concuerdan.Count; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(tepuedeinteresarTable);

                Pelicula pp = (Pelicula)concuerdan[i];

                row.Cells[0].Value = pp.titutlo;
                row.Cells[1].Value = pp.anodefilmacion;
                row.Cells[2].Value = pp.actoresprincipales;
                row.Cells[3].Value = pp.directores;

                tepuedeinteresarTable.Rows.Add(row);
            }
            tepuedeinteresarTable.AllowUserToAddRows = false;
        }

        private void llenarTablaConPeliculas(Pelicula[] peliculas)
        {
            //Se limpian todos los campos del form
            limpiarCampos();

            peliculasDatagrid.ClearSelection();

            //Limpiando la tabla
            peliculasDatagrid.Rows.Clear();

            //Se habilita la posibilidad de añadir registros
            peliculasDatagrid.AllowUserToAddRows = true;

            //Se llena la tabla con las películas ingresadas
            for(int i = 0; i < peliculas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(peliculasDatagrid);
                row.Cells[0].Value = peliculas[i].noserie;
                row.Cells[1].Value = peliculas[i].titutlo;
                row.Cells[2].Value = peliculas[i].actoresprincipales;
                row.Cells[3].Value = peliculas[i].anodefilmacion;
                row.Cells[4].Value = peliculas[i].directores;
                row.Cells[5].Value = peliculas[i].genero;
                row.Cells[6].Value = peliculas[i].tipo;

                peliculasDatagrid.Rows.Add(row);
            }

            //Se vuelve a deshabilitar la opción de añadir filas
            peliculasDatagrid.AllowUserToAddRows = false;
        }

        private void buscarButton_Click(object sender, EventArgs e)
        {
            //SE PROCEDE A HACER LA BÚSQUEDA PARA EL USUARIO
            SqlCommand command = new SqlCommand();
            
            command.CommandText = "SELECT * FROM INVENTARIOPELICULAS2";
            //Se manda a ejecutar el comando
            ArrayPeliculas ap = new ArrayPeliculas();
            if (omitirCheckbox.Checked)
            {
                string[] p = { "@IDSOCIO" };
                object[] v = { StaticClass.socio };
                ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE NOT IN (SELECT NOSERIE FROM RENTAS WHERE IDSOCIO=@IDSOCIO);", p, v);
            }
            else
            {
                ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2;", new string[0], new object[0]);
            }

            //Arraylist para guardar la selección
            ArrayList seleccion = new ArrayList();

            //Se hace la búsqueda dependiendo de lo que haya seleccionado el usuario
            for (int i = 0; i < ap.peliculas.Length; ++i)
            {
                Pelicula p = ap.peliculas[i];
                bool agregar = false;

                if (tituloRadioButton.Checked)
                {
                    agregar = p.titutlo.Contains(textBox.Text);
                }
                else if (generoRadioButton.Checked)
                {
                    agregar = p.genero.Contains(textBox.Text);
                }
                else if (anoRadioButton.Checked)
                {
                    try
                    {
                        int ano = Convert.ToInt32(textBox.Text);
                        agregar = p.anodefilmacion == ano;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Por favor ingrese un año válido (sólo números)");
                        return;
                    }
                }
                else if (actorRadioButton.Checked)
                {
                    agregar = p.actoresprincipales.Contains(textBox.Text);
                }
                else if (directorRadioButton.Checked)
                {
                    agregar = p.directores.Contains(textBox.Text);
                }

                //Si es que cumple con las condiciones entonces se agrega al arraylist
                if (agregar)
                {
                    seleccion.Add(p);
                    //MessageBox.Show("Se ha agregado una película");
                }

                
            }//FOR END

            //Convirtiendo de arraylist a array
            Pelicula[] peliculas = new Pelicula[seleccion.Count];
            for (int i = 0; i < peliculas.Length; ++i)
            {
                peliculas[i] = (Pelicula)seleccion[i];
            }

            //Se procede a llenar la tabla
            llenarTablaConPeliculas(peliculas);
                       
        }

        private void BuscarPeliculas_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void limpiarCampos()
        {
            copiasDisponiblesTextbox.Text = "";
            precioTextbox.Text = "";
            tipoTextbox.Text = "";
            reservarButton.Enabled = false;
        }

        //METODO PARA CUANDO SE SELECCIONA UNA PELICULA Y SE QUIEREN MOSTRAR MÁS CAMPOS DE ELLA
        private void peliculaSeleccionada()
        {

            //Si la tabla no contiene la cantidad suficiente de filas entonces se sale
            if (peliculasDatagrid.Rows.Count <= 0)
            {
                //Se limpian los campos
                limpiarCampos();
                return;
            }

            //Sacar el id del row seleccionado
            DataGridViewRow row = peliculasDatagrid.Rows[peliculasDatagrid.SelectedCells[0].RowIndex];

            //Se procede a obtener el número de copias disponibles para esa pelicula
            string cm;
            cm = "SELECT * FROM COPIAS WHERE IDCOPIA NOT IN (SELECT IDCOPIA FROM BAJAS) AND IDCOPIA NOT IN (SELECT IDCOPIA FROM RENTAS WHERE ESTADO = 'PENDIENTE') AND NOSERIE=@NOSERIE";
            string[] parameters = new string[1];
            string[] values = new string[parameters.Length];

            parameters[0] = "@NOSERIE";
            values[0] = row.Cells[0].Value.ToString();
            
            ArrayCopias copias = client.getCopiasWithQuery(cm, parameters, values);

            //Se muestra el número de copias disponibles
            copiasDisponiblesTextbox.Text = copias.copias.Length+"";


            //Se carga el precio y el tipo que tiene
            cm = "SELECT * FROM COSTOS WHERE TIPO=@TIPO";
            string[] p = {"@TIPO"};
            string[] v = {row.Cells[6].Value.ToString()};
            ArrayCostos ac = client.getCostosWithQuery(cm, p, v);

            //Una vez que ya se tienen los datos se procede a cargar los datos visualmente
            precioTextbox.Text = "$" + ac.costos[0].precio;
            tipoTextbox.Text = ac.costos[0].tipo;

            reservarButton.Enabled = true;

        }

        private void peliculasDatagrid_Click(object sender, EventArgs e)
        {
            peliculaSeleccionada();
        }

        private void peliculasDatagrid_KeyDown(object sender, KeyEventArgs e)
        {
            peliculaSeleccionada();
        }

        private void peliculasDatagrid_KeyUp(object sender, KeyEventArgs e)
        {
            peliculaSeleccionada();
        }

        private void reservarButton_Click(object sender, EventArgs e)
        {
            //Si el socio ya ha reservado tres películas hoy ya no puede reservar otra
            string[] p1 = {"@SOCIO", "@TODAY"};
            object[] v1 = {StaticClass.socio + "", DateTime.Today};
            ArrayReservas ar = client.getReservasWithQuery("SELECT * FROM RESERVAS WHERE SOCIO=@SOCIO AND FECHARESERVA=@TODAY;", p1, v1);

            if (ar.reservas.Length == 3)
            {
                MessageBox.Show("Has alcanzado el numero máximo de reservas por día");
                return;
            }

            //Se intenta reservar una película para el socio

            //El socio no puede reservar la misma película si ya está reservada
            DataGridViewRow row = peliculasDatagrid.Rows[peliculasDatagrid.SelectedCells[0].RowIndex];
            string[] p = {"@NOSERIE", "@SOCIO"};
            object[] v = {row.Cells[0].Value.ToString(), StaticClass.socio+""};
            ar = client.getReservasWithQuery("SELECT * FROM RESERVAS WHERE SOCIO=@SOCIO AND NOSERIE=@NOSERIE", p, v);

            if (ar.reservas.Length > 0)
            {
                MessageBox.Show("Ya tienes una reserva con esta película");
                return;
            }

            //Se obtiene el mayor numero de reserva
            ar = client.getReservasWithQuery("SELECT * FROM RESERVAS WHERE IDRESERVA = (SELECT MAX(IDRESERVA) FROM RESERVAS);", new string[0], new string[0]);
            int idreserva = 0;
            if (ar.reservas.Length == 0)
            {
                idreserva = 1;
            }
            else
            {
                idreserva = ar.reservas[0].idreserva + 1; 
            }
            

            p = new string[]{"@NOSERIE", "@SOCIO", "@FECHARESERVA", "@IDRESERVA"};
            object[] o = new object[] { row.Cells[0].Value.ToString(), StaticClass.socio+"", reservaDatetime.Value, idreserva+""};

            bool resultado = client.executeNonQuery("INSERT INTO RESERVAS (NOSERIE, SOCIO, FECHARESERVA, IDRESERVA) VALUES (@NOSERIE, @SOCIO, @FECHARESERVA, @IDRESERVA);", p,o);

            string message = "";

            if (!resultado)
            {
                message = "Problema al realizar la petición al servidor";
            }
            else
            {
                message = "Reserva realizada exitosamente";
            }

            MessageBox.Show(message);
        }

        private void refrescarButton_Click(object sender, EventArgs e)
        {
            llenarTepuedeinteresar();
        }
    }


}
