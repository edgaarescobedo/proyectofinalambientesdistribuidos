﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class RegistroCopias : Form
    {
        Form padre;
        ProductServiceClient client = new ProductServiceClient();

        public RegistroCopias(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            llenarTablaCopias();
            llenarTablaPeliculas();
        }

        private void RegistroCopias_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }


        private void llenarTablaPeliculas()
        {
            peliculasTable.AllowUserToAddRows = true;

            peliculasTable.Rows.Clear();
            ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2", new string[0], new string[0]);

            for (int i = 0; i < ap.peliculas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(peliculasTable);

                row.Cells[0].Value = ap.peliculas[i].noserie;
                row.Cells[1].Value = ap.peliculas[i].titutlo;

                peliculasTable.Rows.Add(row);
            }

            peliculasTable.AllowUserToAddRows = false;
        }

        private void llenarTablaCopias()
        {
            copiasTable.AllowUserToAddRows = true;

            copiasTable.Rows.Clear();

            ArrayCopias ac = client.getCopiasWithQuery("SELECT * FROM COPIAS", new string[0], new string[0]);
            for (int i = 0; i < ac.copias.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(copiasTable);

                row.Cells[0].Value = ac.copias[i].noserie;
                row.Cells[1].Value = ac.copias[i].idcopia;

                ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE", new string[] { "@NOSERIE" }, new object[] { ac.copias[i].noserie });
                row.Cells[2].Value = ap.peliculas[0].titutlo;

                copiasTable.Rows.Add(row);
            }

            copiasTable.AllowUserToAddRows = false;


            //Se pone en id copias lo necesario para ingresar una nueva copia
            ac = client.getCopiasWithQuery("SELECT * FROM COPIAS WHERE IDCOPIA = (SELECT MAX(IDCOPIA) FROM COPIAS);", new string[0], new string[0]);

            int idcopia = -1;
            if (ac.copias.Length == 0)
            {
                idcopia = 1;
            }
            else
            {
                idcopia = ac.copias[0].idcopia + 1;
            }


            idcopiaTextbox.Text = idcopia+"";
        }

        private void ingresarButton_Click(object sender, EventArgs e)
        {
            if (noserieTextbox.Text == "")
            {
                MessageBox.Show("Por favor ingrese un numero de serie valido");
                return;
            }

            ArrayPeliculas ap = client.getPeliculasWithQuery("SELECT * FROM INVENTARIOPELICULAS2 WHERE NOSERIE=@NOSERIE;",new string[]{"@NOSERIE"}, new object[]{noserieTextbox.Text});
            if (ap.peliculas.Length == 0)
            {
                MessageBox.Show("Por favor ingrese un numero de serie valido");
                return;
            }

            string[] p = {"@IDCOPIA", "@NOSERIE"};
            object[] v = {idcopiaTextbox.Text, noserieTextbox.Text};
            bool resultado = client.executeNonQuery("INSERT INTO COPIAS (IDCOPIA, NOSERIE) VALUES (@IDCOPIA, @NOSERIE);", p, v);

            if (!resultado)
            {
                MessageBox.Show("Problema al ingresar la copia en el servidor");
                return;
            }

            llenarTablaCopias();
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            if (copiasTable.SelectedCells.Count == 0)
            {
                return;
            }

            //Se tiene que verificar que la copia no tenga registros 
            string[] p = {"@IDCOPIA"};
            string[] v = {copiasTable.SelectedCells[0].OwningRow.Cells[1].Value + ""};
            ArrayCopias ac = client.getCopiasWithQuery("SELECT * FROM COPIAS WHERE IDCOPIA NOT IN (SELECT IDCOPIA FROM BAJAS) AND IDCOPIA NOT IN (SELECT IDCOPIA FROM RENTAS) AND IDCOPIA=@IDCOPIA;", p, v);
            if (ac.copias.Length == 0)
            {
                MessageBox.Show("Esta copia ya tiene registros, necesitan ser eliminados antes de eliminar la copia");
                return;
            }

            bool resultado = client.executeNonQuery("DELETE FROM COPIAS WHERE IDCOPIA=@IDCOPIA", p, v);
            if (!resultado)
            {
                MessageBox.Show("Problema al realizar la baja en el servidor");
                return;
            }

            llenarTablaCopias();
        }
    }
}
