﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;

namespace GUIClient
{
    public partial class VerAdeudos : Form
    {
        ProductServiceClient client = new ProductServiceClient();
        Form padre;

        public VerAdeudos(Form padre)
        {
            InitializeComponent();
            this.padre = padre;
            padre.Enabled = false;

            llenarTabla();
        }

        private void VerAdeudos_FormClosing(object sender, FormClosingEventArgs e)
        {
            padre.Enabled = true;
            this.Dispose();
        }

        private void llenarTabla()
        {

            string[] p = {"@IDSOCIO", "@ESTADO"};
            object[] v = {StaticClass.socio, "PENDIENTE"};
            ArrayRentas ar = client.getRentasWithQuery("SELECT * FROM RENTAS WHERE IDSOCIO=@IDSOCIO AND ESTADO=@ESTADO", p, v);

            rentasTable.AllowUserToAddRows = true;
            rentasTable.Rows.Clear();

            for (int i = 0; i < ar.rentas.Length; ++i)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(rentasTable);

                row.Cells[0].Value = ar.rentas[i].idrenta;
                row.Cells[1].Value = ar.rentas[i].fecha;
                row.Cells[2].Value = ar.rentas[i].fechaentrega;
                row.Cells[3].Value = ar.rentas[i].costo;

                DateTime now = DateTime.Now;

                if ((now - ar.rentas[i].fechaentrega).TotalDays > 0)
                {
                    row.Cells[4].Value = "SI";
                }
                else
                {
                    row.Cells[4].Value = "NO";
                }

                rentasTable.Rows.Add(row);
            }

            rentasTable.AllowUserToAddRows = false;
        }
    }

}
