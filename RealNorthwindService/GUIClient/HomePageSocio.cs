﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using GUIClient.ServiceRef;


namespace GUIClient
{
    public partial class HomePageSocio : Form
    {

        ProductServiceClient client = new ProductServiceClient();

        public HomePageSocio()
        {
            InitializeComponent();


            //Se bloqueará al socio si tiene rentas con más de 10 días de retraso
            string[] p = {"@IDSOCIO", "@ESTADO"};
            object[] v = {StaticClass.socio, "PENDIENTE"};
            ArrayRentas ar = client.getRentasWithQuery("SELECT * FROM RENTAS WHERE IDSOCIO=@IDSOCIO AND ESTADO=@ESTADO;", p, v);

            DateTime now = DateTime.Now;

            for (int i = 0; i < ar.rentas.Length; ++i)
            {
                if ((ar.rentas[i].fechaentrega - now).TotalDays >= 10)
                {
                    MessageBox.Show("Tu usuario ha sido bloqueado debido a un adeudo, por favor liquida el adeudo para liberar tu usuario.");
                    reservasButton.Enabled = false;
                    buscarPeliculasButton.Enabled = false;

                }
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Se abre la ventana de inicio
            new Inicio().Show();
            this.Dispose();


          
        }

        private void HomePageSocio_Load(object sender, EventArgs e)
        {

        }

        private void HomePageSocio_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void buscarPeliculasButton_Click(object sender, EventArgs e)
        {
            new BuscarPeliculas(this).Show();
        }

        private void reservasButton_Click(object sender, EventArgs e)
        {
            new Reservas(this).Show();
        }

        private void verAdeudosButton_Click(object sender, EventArgs e)
        {
            new VerAdeudos(this).Show();
        }
    }
}
