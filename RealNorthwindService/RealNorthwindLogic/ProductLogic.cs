﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWCFServices.RealNorthwindBDO;
using MyWCFServices.RealNorthwindDAL;
using System.Collections;

namespace MyWCFServices.RealNorthwindLogic
{
    public class ProductLogic
    {

        ProductDAO productDAO = new ProductDAO();

        public ProductBDO GetProduct(int id)
        {
            return productDAO.GetProduct(id);
        }

        public ArrayList GetAllProducts()
        {
            return productDAO.GetAllProducts();
        }


        public bool UpdateProduct(ProductBDO product,
            ref string message)
            {
            ProductBDO productInDB =
            GetProduct(product.ProductID);
            // invalid product to update
            if (productInDB == null)
            {
            message = "cannot get product for this ID";
            return false;
            }
            // a product can't be discontinued
            // if there are non-fulfilled orders
            if (product.Discontinued == true
            && productInDB.UnitsOnOrder > 0)
            {       
            message = "cannot discontinue this product";
            return false;
            }
            else
            {
                return productDAO.UpdateProduct(product, ref message);
            }
        }


        //Creado para el proyecto
        public SocioBDO getSocio(int id)
        {
            return productDAO.getSocio(id);
        }

        public ArrayList getAllSocios()
        {
            return productDAO.getAllSocios();
        }

        public int getNextSocioID()
        {
            return productDAO.getNextSocioID();
        }

        public ArrayList getSociosWithQuery(string query, string[] parameters, object[] values)
        {
            return productDAO.getSociosWithQuery(query, parameters, values);
        }

        public bool updateSocio(SocioBDO socio)
        {
            return productDAO.updateSocio(socio);
        }
        public bool insertSocio(SocioBDO socio)
        {
            return productDAO.insertSocio(socio);
        }

        public UsuarioBDO getUsuario(string id)
        {
            return productDAO.getUsuario(id);
        }

        public int getNumeroDeCopiasDisponiblesParaPelicula(int id)
        {
            return productDAO.getNumeroDeCopiasDisponiblesParaPelicula(id);
        }

        public ArrayList getPeliculasWithQuery(string query, string[] parameters, object[] values)
        {
            return productDAO.getPeliculasWithQuery(query, parameters, values);
        }

        public ArrayList getCopiasWithQuery(string query, string[] paramters, string[] values)
        {
            return productDAO.getCopiasWithQuery(query, paramters, values);
        }

        public ArrayList getCostosWithQuery(string query, string[] parameters, string[] values)
        {
            return productDAO.getCostosWithQuery(query, parameters, values);
        }

        public ArrayList getBajasWithQuery(string query, string[] parameters, string[] values)
        {
            return productDAO.getBajasWithQuery(query, parameters, values);
        }

        public ArrayList getReservasWithQuery(string query, string[] parameters, object[] values)
        {
            return productDAO.getReservasWithQuery(query, parameters, values);
        }

        public bool executeNonQuery(string query, string[] parameters, object[] values)
        {
            return productDAO.executeNonQuery(query, parameters, values);
        }

        public ArrayList getRentasWithQuery(string query, string[] parameters, object[] values)
        {
            return productDAO.getRentasWithQuery(query, parameters, values);
        }

        public ArrayList getMultasWithQuery(string query, string[] parameters, object[] values)
        {
            return productDAO.getMultasWithQuery(query, parameters, values);
        }

        public ArrayList getUsuarioWithQuery(string query, string[] parameters, object[] values)
        {
            return productDAO.getUsuarioWithQuery(query, parameters, values);
        }
    }

}
