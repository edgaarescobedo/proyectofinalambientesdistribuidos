﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyWCFServices.RealNorthwindBDO
{
    public class ProductBDO
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal UnitPrice { get; set; }
        public int UnitsInStock { get; set; }
        public int ReorderLevel { get; set; }
        public int UnitsOnOrder { get; set; }
        public bool Discontinued { get; set; }
    }

    //Creado para el proyecto final
    public class SocioBDO
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string numero { get; set; }
        public string identificacion { get; set; }
        public DateTime nacimiento { get; set; }
        public string docimicilio { get; set; }
        public DateTime registro { get; set; }
        public string password { get; set; }
    }

    public class UsuarioBDO
    {
        public string id { get; set; }
        public string tipo { get; set; }
        public string password { get; set; }
    }

    public class CopiaBDO
    {
        public int idcopia { get; set; }
        public int noserie { get; set; }
    }

    public class PeliculaBDO
    {
        public int noserie { get; set; }
        public string titutlo { get; set; }
        public int anodefilmacion { get; set; }
        public string directores { get; set; }
        public string actoresprincipales { get; set; }
        public string idiomaoriginal { get; set; }
        public string idiomasdisponibles { get; set; }
        public string genero { get; set; }
        public float duracion { get; set; }
        public string tipo { get; set; }
    }

    public class CostoBDO
    {
        public string tipo { get; set; }
        public float precio { get; set; }
        public int tiemporenta { get; set; }
    }

    public class BajaBDO
    {
        public int idbaja { get; set; }
        public int idcopia { get; set; }
        public string razon { get; set; }
    }

    public class ReservaBDO
    {
        public int noserie { get; set; }
        public int socio { get; set; }
        public DateTime fechareserva { get; set; }
        public int idreserva { get; set; }
    }

    public class RentaBDO
    {
        public int idcopia { get; set; }
        public int idrenta { get; set; }
        public int idsocio { get; set; }
        public int noserie { get; set; }
        public float costo { get; set; }
        public string estado { get; set; }
        public DateTime fecha { get; set; }
        public DateTime fechaentrega { get; set; }
    }

    public class MultaBDO
    {
        public int idmulta { get; set; }
        public int idrenta { get; set; }
        public float multatotal { get; set; }
    }
}
