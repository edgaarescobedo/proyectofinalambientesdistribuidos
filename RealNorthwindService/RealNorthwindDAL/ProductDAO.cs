﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyWCFServices.RealNorthwindBDO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections;


namespace MyWCFServices.RealNorthwindDAL
{
    public class ProductDAO
    {

        string connectionString = ConfigurationManager.ConnectionStrings["NorthwindConnectionString"].ConnectionString;
        
        public ProductBDO GetProduct(int id)
        {
            ProductBDO p = null;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText =
                    "select * from SOCIOS where ID=@id";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            p = new ProductBDO();
                            p.ProductID = id;
                            p.ProductName = (string)reader["ProductName"];
                            p.QuantityPerUnit = (string)reader["QuantityPerUnit"];
                            p.UnitPrice = (decimal)reader["UnitPrice"];
                            p.UnitsInStock = (short)reader["UnitsInStock"];
                            p.UnitsOnOrder = (short)reader["UnitsOnOrder"];
                            p.ReorderLevel = (short)reader["ReorderLevel"];
                            p.Discontinued = (bool)reader["Discontinued"];
                        }
                    }
                }
            }
            return p;
        }

        public ArrayList GetAllProducts() {

            ArrayList array = new ArrayList();

            ProductBDO p = null;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from Products";
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader =
                    cmd.ExecuteReader())
                    {


                        while (reader.HasRows)
                        {
                            reader.Read();
                            p = new ProductBDO();
                            p.ProductID = (int)reader["ProductID"];
                            p.ProductName = (string)reader["ProductName"];
                            p.QuantityPerUnit = (string)reader["QuantityPerUnit"];
                            p.UnitPrice = (decimal)reader["UnitPrice"];
                            p.UnitsInStock = (short)reader["UnitsInStock"];
                            p.UnitsOnOrder = (short)reader["UnitsOnOrder"];
                            p.ReorderLevel = (short)reader["ReorderLevel"];
                            p.Discontinued = (bool)reader["Discontinued"];
                            
                            //Se llenan de uno a uno la lista de productos
                            array.Add(p);

                            //Se salta al siguiente resultado
                            reader.NextResult();
                        }
                    }
                }
            }

            return array;
        }

        public bool UpdateProduct(ProductBDO product, ref string message)
        {
            message = "product updated successfully";
            bool ret = true;
            using (SqlConnection conn =
            new SqlConnection(connectionString))
            {
                string cmdStr = @"UPDATE products
                                SET ProductName=@name,
                                QuantityPerUnit=@unit,
                                UnitPrice=@price,
                                Discontinued=@discontinued
                                WHERE ProductID=@id";
                using (SqlCommand cmd = new SqlCommand(cmdStr, conn))
                {
                    cmd.Parameters.AddWithValue("@name",
                    product.ProductName);
                    cmd.Parameters.AddWithValue("@unit",
                    product.QuantityPerUnit);
                    cmd.Parameters.AddWithValue("@price",
                    product.UnitPrice);
                    cmd.Parameters.AddWithValue("@discontinued",
                    product.Discontinued);
                    cmd.Parameters.AddWithValue("@id",
                    product.ProductID);
                    conn.Open();
                    if (cmd.ExecuteNonQuery() != 1)
                    {
                        message = "no product was updated";
                        ret = false;
                    }
                }
            }
            return ret;
        }


        //Creado para el proyecto final
        public SocioBDO getSocio(int id)
        {
            SocioBDO p = null;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText =
                    "select * from SOCIOS where ID=@id";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            p = new SocioBDO();
                            p.id = id;
                            p.nombre = (string)reader["NOMBRE"];
                            p.numero = (string)reader["NUMERO"];
                            p.identificacion = (string)reader["IDENTIFICACION"];
                            p.nacimiento = reader.GetDateTime(reader.GetOrdinal("NACIMIENTO"));
                            p.docimicilio = (string)reader["DOMICILIO"];
                            p.registro = reader.GetDateTime(reader.GetOrdinal("REGISTRO"));
                            p.password = (string)reader["PASSWORD"];
                        }
                    }
                }
            }
            return p;
        }
        public ArrayList getAllSocios()
        {
            ArrayList array = new ArrayList();
            
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from SOCIOS";
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (reader.Read())
                        {
                            SocioBDO p = new SocioBDO();
                            p.id = (int)reader["ID"];
                            p.nombre = (string)reader["NOMBRE"];
                            p.numero = (string)reader["NUMERO"];
                            p.identificacion = (string)reader["IDENTIFICACION"];
                            p.nacimiento = reader.GetDateTime(reader.GetOrdinal("NACIMIENTO"));
                            p.docimicilio = (string)reader["DOMICILIO"];
                            p.registro = reader.GetDateTime(reader.GetOrdinal("REGISTRO"));
                            p.password = (string)reader["PASSWORD"];
                            array.Add(p);
                        }
                    }
                }
            }
            return array;
        }

        public ArrayList getSociosWithQuery(string query, string[] parameters, object[] values)
        {
            ArrayList array = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = query;

                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }

                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (reader.Read())
                        {
                            SocioBDO p = new SocioBDO();
                            p.id = (int)reader["ID"];
                            p.nombre = (string)reader["NOMBRE"];
                            p.numero = (string)reader["NUMERO"];
                            p.identificacion = (string)reader["IDENTIFICACION"];
                            p.nacimiento = reader.GetDateTime(reader.GetOrdinal("NACIMIENTO"));
                            p.docimicilio = (string)reader["DOMICILIO"];
                            p.registro = reader.GetDateTime(reader.GetOrdinal("REGISTRO"));
                            p.password = (string)reader["PASSWORD"];
                            array.Add(p);
                        }
                    }
                }
            }
            return array;
        }
        public int getNextSocioID()
        {
            SocioBDO p = null;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText =
                    "SELECT * FROM SOCIOS WHERE ID = (SELECT MAX(ID) FROM SOCIOS);";
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            p = new SocioBDO();
                            p.id = (int)reader["ID"];
                            p.nombre = (string)reader["NOMBRE"];
                            p.numero = (string)reader["NUMERO"];
                            p.identificacion = (string)reader["IDENTIFICACION"];
                            p.nacimiento = reader.GetDateTime(reader.GetOrdinal("NACIMIENTO"));
                            p.docimicilio = (string)reader["DOMICILIO"];
                            p.registro = reader.GetDateTime(reader.GetOrdinal("REGISTRO"));
                        }
                    }
                }
            }
            return p.id + 1;
        }


        public UsuarioBDO getUsuario(string id)
        {
            UsuarioBDO p = null;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText =
                    "select * from USUARIOS where ID=@id";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            p = new UsuarioBDO();
                            p.id = id;
                            p.password = (string)reader["PASSWORD"];
                            p.tipo = (string)reader["TIPO"];
                        }
                    }
                }
            }
            return p;
        }

        public bool updateSocio(SocioBDO socio)
        {
            
            bool ret = true;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string cmdStr = "UPDATE SOCIOS SET NOMBRE = @NOMBRE, NUMERO=@NUMERO, IDENTIFICACION=@IDENTIFICACION, NACIMIENTO=@NACIMIENTO, DOMICILIO=@DOMICILIO, REGISTRO=@REGISTRO, PASSWORD=@PASSWORD WHERE ID=@ID";
                    
                using (SqlCommand cmd = new SqlCommand(cmdStr, conn))
                {

                    cmd.Parameters.AddWithValue("@NOMBRE", socio.nombre);
                    cmd.Parameters.AddWithValue("@NUMERO", socio.numero);
                    cmd.Parameters.AddWithValue("@IDENTIFICACION", socio.identificacion);
                    cmd.Parameters.AddWithValue("@NACIMIENTO", socio.nacimiento);
                    cmd.Parameters.AddWithValue("@DOMICILIO", socio.docimicilio);
                    cmd.Parameters.AddWithValue("@REGISTRO", socio.registro);
                    cmd.Parameters.AddWithValue("@ID", socio.id);
                    cmd.Parameters.AddWithValue("@PASSWORD", socio.password);

                    conn.Open();
                    if (cmd.ExecuteNonQuery() != 1)
                    {
                        ret = false;
                    }
                }
            }
            return ret;
        }
        public bool insertSocio(SocioBDO socio)
        {
            bool ret = true;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string cmdStr = "INSERT INTO SOCIOS (ID, NOMBRE, NUMERO, IDENTIFICACION, NACIMIENTO, DOMICILIO, REGISTRO) VALUES (@ID, @NOMBRE, @NUMERO, @IDENTIFICACION, @NACIMIENTO, @DOMICILIO, @REGISTRO, @PASSWORD)";

                using (SqlCommand cmd = new SqlCommand(cmdStr, conn))
                {

                    cmd.Parameters.AddWithValue("@NOMBRE", socio.nombre);
                    cmd.Parameters.AddWithValue("@NUMERO", socio.numero);
                    cmd.Parameters.AddWithValue("@IDENTIFICACION", socio.identificacion);
                    cmd.Parameters.AddWithValue("@NACIMIENTO", socio.nacimiento);
                    cmd.Parameters.AddWithValue("@DOMICILIO", socio.docimicilio);
                    cmd.Parameters.AddWithValue("@REGISTRO", socio.registro);
                    cmd.Parameters.AddWithValue("@ID", socio.id);
                    cmd.Parameters.AddWithValue("@PASSWORD", socio.id);

                    conn.Open();
                    if (cmd.ExecuteNonQuery() != 1)
                    {
                        ret = false;
                    }
                }
            }
            return ret;
        }

        //PARA COPIAS
        public int getNumeroDeCopiasDisponiblesParaPelicula(int id)
        {

            ArrayList copias = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM COPIAS WHERE NOSERIE=@NOSERIE";
                    cmd.Parameters.AddWithValue("@NOSERIE", id);
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();

                            CopiaBDO copia = new CopiaBDO();
                            copia.idcopia = (int)reader["IDCOPIA"];
                            copia.noserie = (int)reader["NOSERIE"];

                            copias.Add(copia);
                        }
                    }
                }
            }
            return copias.Count;
        }
        public ArrayList getCopiasWithQuery(string query, string[] paramters, string[] values)
        {
            ArrayList copias = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se empiezan a agregar los parámetros con los valores
                    for (int i = 0; i < paramters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(paramters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CopiaBDO copia = new CopiaBDO();
                            copia.idcopia = Convert.ToInt32(reader["IDCOPIA"].ToString());
                            copia.noserie = Convert.ToInt32(reader["NOSERIE"].ToString());

                            copias.Add(copia);
                        }
                    }
                }
            }
            return copias;
        }


        //PARA PELICULAS
        public ArrayList getPeliculasWithQuery(string query, string[] parameters, object[] values)
        {
            ArrayList peliculas = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se agregan los parámetros con los valores
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PeliculaBDO pelicula = new PeliculaBDO();
                            pelicula.noserie = (int)reader["NOSERIE"];
                            pelicula.titutlo = reader["TITULO"].ToString();
                            pelicula.anodefilmacion = (int)reader["ANODEFILMACION"];
                            pelicula.directores = reader["DIRECTORES"].ToString();
                            pelicula.actoresprincipales = reader["ACTORESPRINCIPALES"].ToString();
                            pelicula.idiomaoriginal = reader["IDIOMAORIGINAL"].ToString();
                            pelicula.idiomasdisponibles = reader["IDIOMASDISPONIBLES"].ToString();
                            pelicula.genero = reader["GENERO"].ToString();
                            pelicula.duracion = (float)Convert.ToDouble(reader["DURACION"].ToString());
                            pelicula.tipo = reader["TIPO"].ToString();

                            peliculas.Add(pelicula);
                        }
                    }
                }
            }
            return peliculas;
        }

        //PARA COSTOS
        public ArrayList getCostosWithQuery(string query, string[] parameters, string[] values)
        {
            ArrayList costos = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se empiezan a agregar los parámetros con los valores
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CostoBDO costo = new CostoBDO();
                            costo.tipo = reader["TIPO"].ToString();
                            costo.precio = (float)Convert.ToDouble(reader["PRECIO"].ToString());
                            costo.tiemporenta = Convert.ToInt32(reader["TIEMPORENTA"].ToString());
                            
                            costos.Add(costo);
                        }
                    }
                }
            }
            return costos;
        }

        public ArrayList getBajasWithQuery(string query, string[] parameters, string[] values)
        {
            ArrayList bajas = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se empiezan a agregar los parámetros con los valores
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            BajaBDO baja = new BajaBDO();
                            baja.idbaja = Convert.ToInt32(reader["IDBAJA"].ToString());
                            baja.idcopia = Convert.ToInt32(reader["IDCOPIA"].ToString());
                            baja.razon = reader["RAZON"].ToString();

                            bajas.Add(baja);
                        }
                    }
                }
            }
            return bajas;
        }

        //RESERVAS
        public ArrayList getReservasWithQuery(string query, string[] parameters, object[] values)
        {
            ArrayList reservas = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se empiezan a agregar los parámetros con los valores
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ReservaBDO r = new ReservaBDO();
                            r.noserie = Convert.ToInt32(reader["NOSERIE"].ToString());
                            r.socio = Convert.ToInt32(reader["SOCIO"].ToString());
                            r.fechareserva = reader.GetDateTime(reader.GetOrdinal("FECHARESERVA"));
                            r.idreserva = Convert.ToInt32(reader["IDRESERVA"].ToString());

                            reservas.Add(r);
                        }
                    }
                }
            }
            return reservas;
        }


        //PARA RENTAS
        public ArrayList getRentasWithQuery(string query, string[] parameters, object[] values)
        {
            ArrayList rentas = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se empiezan a agregar los parámetros con los valores
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            RentaBDO r = new RentaBDO();
                            r.idcopia = Convert.ToInt32(reader["IDCOPIA"].ToString());
                            r.idrenta = Convert.ToInt32(reader["IDRENTA"].ToString());
                            r.idsocio = Convert.ToInt32(reader["IDSOCIO"].ToString());
                            r.noserie = Convert.ToInt32(reader["NOSERIE"].ToString());
                            r.costo = (float)Convert.ToDouble(reader["COSTO"].ToString());
                            r.estado = reader["ESTADO"].ToString();
                            r.fecha = reader.GetDateTime(reader.GetOrdinal("FECHA"));
                            r.fechaentrega = reader.GetDateTime(reader.GetOrdinal("FECHAENTREGA"));

                            rentas.Add(r);
                        }
                    }
                }
            }
            return rentas;
        }

        //PARA MULTAS
        public ArrayList getMultasWithQuery(string query, string[] parameters, object[] values)
        {
            ArrayList multas = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se empiezan a agregar los parámetros con los valores
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MultaBDO m = new MultaBDO();
                            m.idmulta = Convert.ToInt32(reader["IDMULTA"].ToString());
                            m.idrenta = Convert.ToInt32(reader["IDRENTA"].ToString());
                            m.multatotal = (float)Convert.ToDouble(reader["MULTATOTAL"].ToString());

                            multas.Add(m);
                        }
                    }
                }
            }
            return multas;
        }

        //PARA USUARIOS
        public ArrayList getUsuarioWithQuery(string query, string[] parameters, object[] values)
        {
            ArrayList usuarios = new ArrayList();

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = query;
                    //Se empiezan a agregar los parámetros con los valores
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UsuarioBDO usuario = new UsuarioBDO();
                            usuario.id = reader["ID"].ToString();
                            usuario.password = reader["PASSWORD"].ToString();
                            usuario.tipo = reader["TIPO"].ToString();

                            usuarios.Add(usuario);
                        }
                    }
                }
            }
            return usuarios;
        }

        //EJECUCIÓN
        public bool executeNonQuery(string query, string[] parameters, object[] values)
        {
            bool ret = true;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {


                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    
                    for (int i = 0; i < parameters.Length; ++i)
                    {
                        cmd.Parameters.AddWithValue(parameters[i], values[i]);
                    }

                    conn.Open();
                    if (cmd.ExecuteNonQuery() != 1)
                    {
                        ret = false;
                    }
                }
            }
            return ret;
        }
    }
}
